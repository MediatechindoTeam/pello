//
//  NSString+StringHelper.m
//  Dreammary
//
//  Created by Virendra Jethwa on 1/3/14.
//  Copyright (c) 2014 Virendra Jethwa. All rights reserved.
//

#import "NSString+StringHelper.h"

@implementation NSString (StringHelper)
#define IOS_NEWER_OR_EQUAL_TO_7 ( [ [ [ UIDevice currentDevice ] systemVersion ] floatValue ] >= 7.0)

- (CGFloat)getHeightOfTextForFontSize:(CGFloat)size withLabelWidth:(CGFloat)width {
    //Calculate the expected size based on the font and linebreak mode of the label
//    CGFloat maxWidth = 292;
    CGFloat maxHeight = CGFLOAT_MAX;    // was 9999
    CGSize maximumLabelSize = CGSizeMake(width,maxHeight);
    
   // UIFont *myFont=[UIFont fontWithName:@"Helvetica" size:size];
    UIFont *myFont=[UIFont systemFontOfSize:size];
    CGSize expectedLabelSize;
    if(IOS_NEWER_OR_EQUAL_TO_7){
        /*
        NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                              myFont, NSFontAttributeName,
                                              nil];*/
        
        NSMutableDictionary *attributesDictionary = [[NSMutableDictionary alloc] init];
        [attributesDictionary setObject:myFont forKey:NSFontAttributeName];
        
        CGRect frame = [self boundingRectWithSize:maximumLabelSize
                                          options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                       attributes:attributesDictionary
                                          context:nil];
        
        expectedLabelSize =  frame.size;
    }else{
        expectedLabelSize = [self sizeWithFont:[UIFont systemFontOfSize:size] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByWordWrapping];
    }
//    
//    if (expectedLabelSize.height <=40) {
//        return 40;
//    }
//    else {
//        return expectedLabelSize.height;
//    }
    return expectedLabelSize.height;
}



@end
