

#import "NSDictionary+HAAddition.h"

@implementation NSDictionary (HAAddition)

- (NSDictionary *) dictionaryByReplacingNullsWithStrings
{
    const NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:self];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    
    for (NSString *key in self) {
        const id object = [self objectForKey: key];
        if (object == nul) {
            [replaced setObject: blank forKey: key];
        }
        else if ([object isKindOfClass: [NSDictionary class]])
        {
            [replaced setObject: [(NSDictionary *) object dictionaryByReplacingNullsWithStrings] forKey: key];
        }
        else if ([object isKindOfClass: [NSArray class]])
        {
            if([object isEqual:[NSNull null]])
            {
                for (int i = 0; i< [object count]; i++)
                {
                    NSDictionary* dict = [object objectAtIndex:i];
                    dict = [(NSDictionary *) dict dictionaryByReplacingNullsWithStrings];
                    [object replaceObjectAtIndex:i withObject:dict];
                }
            }
            [replaced setObject: object forKey: key];

        }
    }
    return [replaced copy];
}

- (id) dictionaryAndArrayByReplacingNullsWithStrings
{
//    const id replaced = nil;
    if ([self isKindOfClass:[NSArray class]] || [self isKindOfClass:[NSMutableArray class]])
    {
        const NSMutableArray *replaced = [NSMutableArray arrayWithArray:(NSArray*)self];
        //        replaced = [NSMutableArray arrayWithArray:self];
        for (int i = 0; i< [replaced count]; i++)
        {
            NSDictionary* dict = [replaced objectAtIndex:i];
            dict = [(NSDictionary *) dict dictionaryByReplacingNullsWithStrings];
            [replaced replaceObjectAtIndex:i withObject:dict];
        }
        return [replaced copy];
    }
    else
    {
        const NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:self];
        const id nul = [NSNull null];
        const NSString *blank = @"";
        
        for (NSString *key in self) {
            const id object = [self objectForKey: key];
            if (object == nul) {
                [replaced setObject: blank forKey: key];
            }
            else if ([object isKindOfClass: [NSDictionary class]])
            {
                [replaced setObject: [(NSDictionary *) object dictionaryByReplacingNullsWithStrings] forKey: key];
            }
            else if ([object isKindOfClass: [NSArray class]])
            {
                for (int i = 0; i< [object count]; i++)
                {
                    NSDictionary* dict = [object objectAtIndex:i];
                    dict = [(NSDictionary *) dict dictionaryByReplacingNullsWithStrings];
                    [object replaceObjectAtIndex:i withObject:dict];
                }
                [replaced setObject: object forKey: key];
                
            }
        }
        return [replaced copy];
    }
}

@end
