//
//  Connection.h
//  scatterwall
//
//  Created by Himanshu Jadav on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "Reachability.h"
@protocol ConnectionDelegate <NSObject>

- (void)ConnectionDidFinish: (NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode;
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData;

@end

@interface Connection : NSObject
{
    NSString * state;
@private
	id<ConnectionDelegate> connectionDelegate;
    NSMutableData*mutableData;
    NSURLConnection*conn;
}
@property (nonatomic, strong) id connectionDelegate;
@property (nonatomic, strong) NSString * state;

//@property (nonatomic, strong) NSMutableData*mutableData;

+ (Connection*)sharedConnectionWithDelegate:(id)delegate;
+ (Connection*)sharedConnection;
+ (BOOL)isConnected;
-(void)loginWithData:(NSDictionary*)dataDict;
-(void)forgotPasswordWithData:(NSDictionary*)dataDict;
-(void)registerWithData:(NSDictionary*)dataDict;
-(void)registerWithFacebookData:(NSDictionary*)dataDict;
-(void)postStatusWithImage:(NSDictionary *)dataDict;
-(void)getReverseGeocodingFromGoogle:(NSString *)string;
-(void)getFeedList:(NSDictionary*)dataDict;
-(void)likeDislike:(NSDictionary*)dataDict;
-(void)locationData:(NSDictionary*)dataDict;
-(void)GetComment:(NSDictionary*)dataDict;
-(void)AddComment:(NSDictionary*)dataDict;
-(void)DeleteComment:(NSDictionary*)dataDict;
-(void)getNotificationList:(NSDictionary*)dataDict;
-(void)getExplorerList:(NSDictionary*)dataDict;
-(void)getSinglePostData:(NSDictionary*)dataDict;
-(void)findPeopleUsingName:(NSDictionary*)dataDict;
-(void)searchUsingHashtag:(NSDictionary*)dataDict;
-(void)getPostOfUser:(NSDictionary*)dataDict;
-(void)getTaggedPostOfUser:(NSDictionary*)dataDict;
-(void)followingList:(NSDictionary*)dataDict;
-(void)followerList:(NSDictionary*)dataDict;
-(void)getUserDetail:(NSDictionary*)dataDict;
-(void)FollowUnFollowUser:(NSDictionary*)dataDict;
-(void)blockUser:(NSDictionary*)dataDict;
-(void)editProfileDetail:(NSDictionary*)dataDict;
-(void)changePassword:(NSDictionary *)dataDict;
-(void)blockUserList:(NSDictionary*)dataDict;
-(void)getFBFriendList:(NSDictionary *)dict;
-(void)notificationCount:(NSDictionary*)dataDict;
-(void)seenNotificationCount :(NSDictionary *)dict;
-(void)deletePost:(NSDictionary *)dict;
-(void)postReportAbuse:(NSDictionary*)dataDict;
-(NSString*)getCurrentRequest;

- (void)GetComment:(NSDictionary*)dataDict forPhotoID:(NSString*)photoID;
- (void)postStatusWithVideo:(NSDictionary *)dataDict;
- (void)postReportPost:(NSDictionary*)dataDict;
- (void)getReportCategory:(NSDictionary*)dataDict;
- (void)getLikerOfPost:(NSDictionary*)dataDict;
- (void)getPostHashtagOfUser:(NSDictionary*)dataDict;

@end

