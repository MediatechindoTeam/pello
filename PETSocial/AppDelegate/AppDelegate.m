//
//  AppDelegate.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/4/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import "PostVC.h"


@implementation AppDelegate

@synthesize imagefooter,mainTab,viewNewNotification,lblNotificationCount,alertTimer,isNotificationCount,FBConnectionStatus;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    _isNewFeed=NO;
    _isProfileEdited=NO;
    // Override point for customization after application launch.
    
    self.remoteNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    [self tabbarImage];
    [FBAppCall handleDidBecomeActive];
    self.window.backgroundColor = [UIColor blackColor];
    
    
    //-- Set Notification
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }

    if ([[USERDEFAULTS objectForKey:KEYISLOGEDIN] isEqualToString:KEYLOGEDINYES]) {
        alertTimer=[NSTimer scheduledTimerWithTimeInterval:15.0
                                                    target:self
                                                  selector:@selector(NewPostandNotifications:)
                                                  userInfo:nil
                                                   repeats:YES];
        [self notificationViewSetup];
    }
    
    return YES;
}
#pragma mark NOtificationCount Methods
-(void)notificationViewSetup{
    
    CGRect frameViewNewNotification;
    CGRect frameLabelCount;
    
    CGFloat fltMultiplyingFactor = self.window.frame.size.width / 320;
    
    frameViewNewNotification=CGRectMake(235.0 * fltMultiplyingFactor , self.window.frame.size.height-48.0, 16.0, 16.0);
    frameLabelCount =CGRectMake(240.0 * fltMultiplyingFactor, self.window.frame.size.height-46.0, 12, 12);
    
    lblNotificationCount =[[UILabel alloc]initWithFrame:frameLabelCount];
    lblNotificationCount.text =@"";
    
    lblNotificationCount.font =[UIFont fontWithName:@"HelveticaNeue-Bold" size:10];
    lblNotificationCount.textColor =[UIColor whiteColor];
    viewNewNotification =[[UIImageView alloc]initWithFrame:frameViewNewNotification];
    viewNewNotification.image=[UIImage imageNamed:@"circle_notification"];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:viewNewNotification];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:lblNotificationCount];
    viewNewNotification.hidden=NO;
    lblNotificationCount.hidden=NO;
}

-(void)NewPostandNotifications:(NSTimer *)timer
{
    NSDictionary *dataDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",
                              @"notification_count",@"action",
                              nil];
    NSData *data = [NSJSONSerialization dataWithJSONObject:dataDict options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSString *urlString = [BASE_URL stringByAppendingString:APINotificationCount];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([[responseObject valueForKey:@"IsSuccess"] boolValue]==YES )
        {
            NSDictionary *dataArray =[responseObject objectForKey:@"data"] ;
            
            if ([dataArray objectForKey:@"notification_count"] ) {
                NSInteger notificaitonCount=[[dataArray objectForKey:@"notification_count"] integerValue];
                
                if (notificaitonCount!=0) {
                    lblNotificationCount.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:8];
                    isNotificationCount=YES;
                    lblNotificationCount.text=[NSString stringWithFormat:@"%ld",(long)notificaitonCount];
                    viewNewNotification.hidden=NO;
                    lblNotificationCount.hidden=NO;
                    [[[[UIApplication sharedApplication] delegate] window] bringSubviewToFront:viewNewNotification];
                    [[[[UIApplication sharedApplication] delegate] window] bringSubviewToFront:lblNotificationCount];
                }
            }
        }
        else{
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", [error localizedDescription]);
    }];
    [operation start];
    
    
    /*AFHTTPRequestOperationManager *objMnaager = [AFHTTPRequestOperationManager manager];
    //objMnaager.responseSerializer = [AFJSONResponseSerializer serializer];
    objMnaager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *urlString = [BASE_URL stringByAppendingString:APINotificationCount];
    [objMnaager POST:urlString parameters:dataDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *error;
        NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:operation.responseData options:kNilOptions error:&error];
        
        NSLog(@"Notif URL: %@", urlString);
        if ([[dictResponse valueForKey:@"IsSuccess"] boolValue]==YES )
        {
            NSDictionary *dataArray =[dictResponse objectForKey:@"data"] ;
            
            if ([dataArray objectForKey:@"notification_count"] ) {
                NSInteger notificaitonCount=[[dataArray objectForKey:@"notification_count"] integerValue];
                
                if (notificaitonCount!=0) {
                    lblNotificationCount.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:8];
                    isNotificationCount=YES;
                    lblNotificationCount.text=[NSString stringWithFormat:@"%ld",(long)notificaitonCount];
                    viewNewNotification.hidden=NO;
                    lblNotificationCount.hidden=NO;
                    [[[[UIApplication sharedApplication] delegate] window] bringSubviewToFront:viewNewNotification];
                    [[[[UIApplication sharedApplication] delegate] window] bringSubviewToFront:lblNotificationCount];
                }
            }
        }
        else{
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self.alertTimer invalidate];
        self.alertTimer=nil;
        
    }];*/
}

#pragma mark Pushnotification method
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString * strDeviceToken = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    strDeviceToken = [strDeviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    [USERDEFAULTS setObject:strDeviceToken forKey:@"Devicetoken"];
    NSLog(@"Device Token: %@", strDeviceToken);
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    [USERDEFAULTS setObject:@"" forKey:@"Devicetoken"];
    NSLog(@"Failed to get device Token: %@", error);
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    self.remoteNotif = userInfo;
    
    if(application.applicationState == UIApplicationStateActive) {
        
    }
    //if applicaiton is inactive state.
    else if(application.applicationState ==UIApplicationStateInactive){
        [self.mainTab setSelectedIndex:3];
        [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab4"]];
    }
    
    
}
#pragma mark HUD Methods -

-(void)ShowHUDWith:(NSString*)strtitle
{
    [SVProgressHUD showWithStatus:strtitle maskType:SVProgressHUDMaskTypeGradient];
}

-(void)ShowHUDForAlertWithErrorTitel:(NSString*)strtitle
{
    [SVProgressHUD showErrorWithStatus:strtitle];
}

-(void)showHudForAlertWithSuccessTitle:(NSString*)strtitle
{
    [SVProgressHUD showSuccessWithStatus:strtitle];
}

#pragma mark TabBar Related methods
-(void)tabbarImage
{
    
    imagefooter=[[UIImageView alloc] init];
    imagefooter.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    
    [imagefooter setImage:[UIImage imageNamed:@"tab1"]];
    imagefooter.frame = CGRectMake(0,self.window.frame.size.height - imagefooter.image.size.height,self.window.frame.size.width,imagefooter.image.size.height);
    
}
-(void)pushTabBar:(UINavigationController *)navController
{
    mainTab = [[UIStoryboard defaultStoryboard] instantiateViewControllerWithIdentifier:@"TabBar"];
    mainTab.delegate=self;
    [self setimage:0];
    [mainTab.view addSubview:imagefooter];
    [navController pushViewController:mainTab animated:NO];
    
}

#pragma mark - Tab Delegate Method

-(BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    NSInteger index = [[tabBarController viewControllers] indexOfObject:viewController];
    switch (index) {
        case 0:{
            [self setimage:0];
            return YES;
        }
            break;
        case 1:{
            [self setimage:1];
            return YES;
        }
            break;
        case 2:{
            [self setimage:2];
            
            return YES;
        }
        case 3:{
            [self setimage:3];
            return YES;
        }case 4:{
            [self setimage:4];
            return YES;
        }
        default:
            return YES;
            break;
    }
}


-(void)setimage:(NSInteger)selectedindex
{
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
        if(selectedindex==0){
            [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab1"]];
        }
        else if(selectedindex==1){
            [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab2"]];
        }
        else if(selectedindex==2){
            [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab3"]];
        }
        else if(selectedindex==3){
            [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab4"]];
        }
        else if(selectedindex==4){
            [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
        }
        
    }
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    if ([viewController isKindOfClass:[UINavigationController class]]) {
        [(UINavigationController *)viewController popToRootViewControllerAnimated:NO];
    }
}


#pragma mark Facebook Methods
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    //
    switch (state) {
        case FBSessionStateOpen:
            if (!error)
            {
                NSString *strFBToken = [[session accessTokenData] accessToken];
                [[NSUserDefaults standardUserDefaults] setObject:strFBToken forKey:@"FacebookToken"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:FBSessionStateChangedNotification
                 object:session];
            }
            else{
            }
            break;
        case FBSessionStateClosed:
            [SVProgressHUD dismiss];
            
        case FBSessionStateClosedLoginFailed:
            [SVProgressHUD dismiss];
            [FBSession.activeSession closeAndClearTokenInformation];
            break;
        default:
            break;
    }
}

- (void) openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    
    NSArray *permissions = [[NSArray alloc] initWithObjects:@"email",@"user_location",
                            nil];
    
    [self performSelector:@selector(performLogin:) withObject:permissions afterDelay:0.5];
    return;
}
-(void)clearAllUserInfo{
    [FBSession.activeSession closeAndClearTokenInformation];
    [FBSession renewSystemCredentials:^(ACAccountCredentialRenewResult result, NSError *error) {
    }];
    [FBSession setActiveSession:nil];
}
-(void)performLogin:(NSArray*)permissions
{
    
    FBSessionStateHandler completionHandler = ^(FBSession *session, FBSessionState status, NSError *error) {
        [self sessionStateChanged:session state:status error:error];
    };
    
    if ([FBSession activeSession].state == FBSessionStateCreatedTokenLoaded) {
        // we have a cached token, so open the session
        [[FBSession activeSession] openWithBehavior:FBSessionLoginBehaviorUseSystemAccountIfPresent
                                  completionHandler:completionHandler];
    } else {
        [self clearAllUserInfo];
        // create a new facebook session
        FBSession *fbSession = [[FBSession alloc] initWithPermissions:permissions];
        [FBSession setActiveSession:fbSession];
        [fbSession openWithBehavior:FBSessionLoginBehaviorUseSystemAccountIfPresent
                  completionHandler:completionHandler];
    }
    
}

- (void) closeSession
{
    [FBSession.activeSession closeAndClearTokenInformation];
}
- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}
- (void)handleAuthError:(NSError *)error
{
    NSString *alertText;
    NSString *alertTitle;
    if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
        // Error requires people using you app to make an action outside your app to recover
        alertTitle = ALERTTITLE;
        alertText = [FBErrorUtility userMessageForError:error];
        [self showMessage:alertText withTitle:alertTitle];
        
    } else {
        // You need to find more information to handle the error within your app
        if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
            //The user refused to log in into your app, either ignore or...
            alertTitle = ALERTTITLE;
            alertText = @"You need to login to access this part of the app";
            [self showMessage:alertText withTitle:alertTitle];
            
        } else {
            // All other errors that can happen need retries
            // Show the user a generic error message
            alertTitle = ALERTTITLE;
            alertText = @"Please retry";
            [self showMessage:alertText withTitle:alertTitle];
        }
    }
}


-(void) openFbSession:(myCompletion) compblock
{
    [APPDELEGATE openFBSession:^(BOOL finished) {
        compblock(YES);
    }];
    
}


-(void)openFBSession:(myCompletion) compblock
{
    BOOL haveIntegratedFacebookAtAll = ([SLComposeViewController class] != nil);
    self.userHaveIntegrataedFacebookAccountSetup = haveIntegratedFacebookAtAll && ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]);
    if (self.userHaveIntegrataedFacebookAccountSetup)
    {
        //        NSArray *permissions = [NSArray arrayWithObjects:@"read_stream", nil];
        NSArray *permissions = [NSArray arrayWithObjects:@"publish_stream", nil];
        
        [FBSession openActiveSessionWithPublishPermissions:permissions
                                           defaultAudience:FBSessionDefaultAudienceEveryone
                                              allowLoginUI:YES
                                         completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                             
                                             if (error)
                                             {
                                                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERTTITLE
                                                                                                 message:@"Failed to integrate Facebook account."
                                                                                                delegate:nil
                                                                                       cancelButtonTitle:@"OK"
                                                                                       otherButtonTitles:nil];
                                                 [alert show];
                                                 
                                             }
                                             else if (FB_ISSESSIONOPENWITHSTATE(status))
                                             {
                                                 compblock(YES);
                                             }
                                             
                                             
                                         }];
        
    }
    
    
}

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

- (BOOL)openSessionWithFriends:(BOOL)allowLoginUI {
    
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"user_friends",
                            nil];
    if (FBSession.activeSession.isOpen) {
        allowLoginUI=NO;
    }
    else{
        allowLoginUI=YES;
    }
    return [FBSession openActiveSessionWithReadPermissions:permissions
                                              allowLoginUI:allowLoginUI
                                         completionHandler:^(FBSession *session,
                                                             FBSessionState state,
                                                             NSError *error) {
                                             if (!error) {
                                                 [[NSNotificationCenter defaultCenter]
                                                  postNotificationName:FBFindFriendsNotification
                                                  object:session];
                                             }
                                             else{
                                                 [SVProgressHUD dismiss];
                                             }
                                         }];
}



#pragma mark Connection Delegate methods

- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        
        if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATENOTIFICATIONCOUNT]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    NSDictionary *dataArray =[dataDict objectForKey:@"data"] ;
                    
                    if ([dataArray objectForKey:@"notification_count"] ) {
                        NSInteger notificaitonCount=[[dataArray objectForKey:@"notification_count"] integerValue];

                        if (notificaitonCount!=0) {
                            lblNotificationCount.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:8];
                            isNotificationCount=YES;
                            lblNotificationCount.text=[NSString stringWithFormat:@"%ld",(long)notificaitonCount];
                            viewNewNotification.hidden=NO;
                            lblNotificationCount.hidden=NO;
                            [[[[UIApplication sharedApplication] delegate] window] bringSubviewToFront:viewNewNotification];
                            [[[[UIApplication sharedApplication] delegate] window] bringSubviewToFront:lblNotificationCount];
                        }
                    }
                }
                else{
                    
                }
            } else {
                
            }
            
        }
    }
    @catch (NSException *exception)
    {
    }
    
    
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
    [self.alertTimer invalidate];
    self.alertTimer=nil;
}

/*
 * If we have a valid session at the time of openURL call, we handle
 * Facebook transitions by passing the url argument to handleOpenURL
 */
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    
    //  return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                    fallbackHandler:^(FBAppCall *call) {
                    }];
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end