//
//  NotificationCell.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/26/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgUserOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *imgPetOutlet;
@property (weak, nonatomic) IBOutlet UILabel *lblInfoOutlet;
@property (weak, nonatomic) IBOutlet UILabel *lblUSername;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgLikeComment;

@end
