//
//  ExploreCell.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/26/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExploreCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *petImageOutlet;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *grayIndicatorOutlet;

@end
