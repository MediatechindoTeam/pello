//
//  ExploreVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/12/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "ExploreVC.h"
#import "ExploreCell.h"
#import "SinglePostVC.h"


@interface ExploreVC ()
{
    NSMutableArray *explorerDataArray;
    BOOL isMoreData;
    BOOL isValidateData;
    UIRefreshControl *refreshControl;
}
@end

@implementation ExploreVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self preferredStatusBarStyle];
    explorerDataArray=[[NSMutableArray alloc]init];
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    [self callExplorerData:1];
    [self setUpPulltoRefreshView];
    
}

-(void)setUpPulltoRefreshView{
    refreshControl= [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl setTintColor:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]];
    
    NSDictionary *refreshAttributes = @{
                                        NSForegroundColorAttributeName: [UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],
                                        };
    NSString *s = @"Refreshing...";
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:s];
    
    [attributeString setAttributes:refreshAttributes range:NSMakeRange(0, attributeString.length)];
    refreshControl.attributedTitle=attributeString;
    
    
    [refreshControl addTarget:self action:@selector(refershControlAction) forControlEvents:UIControlEventValueChanged];
    [self.collectionOutview addSubview:refreshControl];
    self.collectionOutview.alwaysBounceVertical = YES;
}
-(void)refershControlAction{
    isValidateData=NO;
    [self callExplorerData:1];
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [SVProgressHUD dismiss];
        
        [refreshControl endRefreshing];
    });
    
}
-(void)viewWillAppear:(BOOL)animated{
    
}
-(void)callExplorerData:(int )pageId{
    //
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",[NSString stringWithFormat:@"%d",pageId],@"page", nil];
    [[Connection sharedConnectionWithDelegate:self]getExplorerList:dict];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSIndexPath *indexPath=(NSIndexPath *)sender;
    if ([segue.identifier isEqualToString:@"SinglePostVC"]) {
        SinglePostVC *singlePost=[segue destinationViewController];
        singlePost.postDataDetail=[explorerDataArray objectAtIndex:indexPath.item];
    }
}

#pragma mark UICollectionView Datasource and Delegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==explorerDataArray.count && explorerDataArray.count>collectionItems) {
        
        CGFloat fltMultiplyingFactor = [APPDELEGATE window].frame.size.width / 320;
        
        
        return CGSizeMake(300.f * fltMultiplyingFactor, 50.f * fltMultiplyingFactor);
    }
    else{
        CGFloat fltMultiplyingFactor = [APPDELEGATE window].frame.size.width / 320;
        return CGSizeMake(100.f * fltMultiplyingFactor, 100.f);
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (explorerDataArray.count) {
        if (isMoreData) {
            return explorerDataArray.count+1;
        }
        else{
            return explorerDataArray.count;
        }
    }
    else{
        return 0;
    }
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.item==explorerDataArray.count)
    {
        ExploreCell *cell = (ExploreCell *)[self.collectionOutview dequeueReusableCellWithReuseIdentifier:@"SeeMoreCell" forIndexPath:indexPath];
        [cell.grayIndicatorOutlet startAnimating];
        cell.grayIndicatorOutlet.frame=CGRectMake(cell.frame.size.width/2, cell.frame.size.height/2, cell.grayIndicatorOutlet.frame.size.width, cell.grayIndicatorOutlet.frame.size.height);
        return cell;
        
    }
    else{
        ExploreCell *myCell = (ExploreCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"MyCell"
                                                                                       forIndexPath:indexPath];
        NSDictionary *dictData=[explorerDataArray objectAtIndex:indexPath.row];
        NSString *strimageURL=[NSString stringWithFormat:@"%@",[dictData objectForKey:@"post_image"]];
        NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
        [myCell.petImageOutlet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"thumbnail_200"] success:nil failure:nil];
        
        return myCell;
        
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"SinglePostVC" sender:indexPath];
}

- (IBAction)searchBtnAction:(UIButton *)sender {
    // search thing
}
#pragma mark Pagination
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    float reload_distance = 10;
    if(y > h + reload_distance)
    {
        if (self.newPage>0 && isMoreData) {
            isMoreData=NO;
            isValidateData=YES;
            [self callExplorerData:self.newPage];
        }
        
        //Put your load more data method here...
    }
    
}

#pragma mark Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEEXPLORER]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    if (!isValidateData) {
                        [refreshControl endRefreshing];
                        explorerDataArray=[[NSMutableArray alloc]init];
                        explorerDataArray=[dataDict objectForKey:@"data"];
                        
                    }
                    else{
                        explorerDataArray=[[explorerDataArray arrayByAddingObjectsFromArray:[dataDict objectForKey:@"data"]] mutableCopy];
                    }
                    self.newPage=[[dataDict objectForKey:@"page"] intValue];
                    if (self.newPage>1) {
                        isMoreData=YES;
                    }
                    [self.collectionOutview reloadData];
                }
                else{
                    [refreshControl endRefreshing];
                    
                }
                
                [SVProgressHUD dismiss];
            }
            
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
        [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString *)nState Data: (NSString *)nData{
    [refreshControl endRefreshing];
    [SVProgressHUD dismiss];
}

@end
