//
//  SinglePostVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 7/11/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "SinglePostVC.h"
//#import "FeedViewCell.h"
#import "UserProfileVC.h"
#import "TaggedUsersViewController.h"
#import "HashTagViewController.h"
#import "SinglePostTableViewCell.h"
#import "CustomUtilities.h"
#import "ReportVC.h"

#import "CommentVC.h"
#import "SharingActionSheet.h"
#import "LocationVC.h"
#import "UserProfileVC.h"
#import "TaggedUsersViewController.h"
#import "HashTagViewController.h"
#import "FeedHeaderView.h"
#import "FeedCommentCell.h"
#import "FeedCommentEmptyCell.h"
#import "CommentCell.h"
#import "FeedCommetViewMoreTableViewCell.h"
#import "LikedViewController.h"
#import "KLCPopup.h"
#import "LikerListViewController.h"
#import "TTTAttributedLabel.h"

#import "Constant.h"

#define kMinFoldCommentCount 5
#define kVIDEO_CELL_SIZE 373
#define kCOMMENT_FONT_SIZE 14

#define kTypePhoto @"photo"
#define kTypeVideo @"video"

@interface SinglePostVC ()
{
    NSString *strUserId;
    
    UIView *viewFooter;
    UIActivityIndicatorView *pageLoader;
    BOOL isMoreData;
    BOOL isValidateData;
    int mainPageId;
    UITableViewController *tableViewController;
    NSIndexPath *commentIndexPath;
    NSIndexPath *indexPathRef;
    BOOL isLoadedCall;
}
@end

@implementation SinglePostVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // _tableViewOutlet.dataSource = nil;
    [self preferredStatusBarStyle];
    
    if (self.postDataDetail) {
        if ([self.postDataDetail objectForKey:@"photo_id"]) {
            self.typeItem = kTypePhoto;
        } else {
            self.typeItem = kTypeVideo;
        }
    }
    
    //calling a service for getting full data of post
    
    self.commentArray = [[NSMutableArray alloc] init];
    //UINib * singleCommentCell = [UINib nibWithNibName:@"SinglePostTableViewCell" bundle:nil];
    //[self.tableViewOutlet registerNib:singleCommentCell forCellReuseIdentifier:@"singleCommentCell"];
    
    self.typeItem = @"photo";
    
    [self preferredStatusBarStyle];
    self.newPage=1;

    
    UIRefreshControl *refreshControl = [UIRefreshControl.alloc init];
    [refreshControl setTintColor:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]];
    
    NSDictionary *refreshAttributes = @{
                                        NSForegroundColorAttributeName: [UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],
                                        };
    NSString *s = @"Refreshing...";
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:s];
    
    [attributeString setAttributes:refreshAttributes range:NSMakeRange(0, attributeString.length)];
    refreshControl.attributedTitle=attributeString;
    [refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl=refreshControl;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePostReloadTable) name:DeletePost object:nil];
    
    UINib * feedCommentCell = [UINib nibWithNibName:@"FeedCommentCell" bundle:nil];
    [self.tableViewOutlet registerNib:feedCommentCell forCellReuseIdentifier:@"FeedCommentCell"];
    
    UINib * feedCommentEmptyCell = [UINib nibWithNibName:@"FeedCommentEmptyCell" bundle:nil];
    [self.tableViewOutlet registerNib:feedCommentEmptyCell forCellReuseIdentifier:@"FeedCommentEmptyCell"];
    
    UINib * feedCommentViewMoreCell = [UINib nibWithNibName:@"FeedCommetViewMoreTableViewCell" bundle:nil];
    [self.tableViewOutlet registerNib:feedCommentViewMoreCell forCellReuseIdentifier:@"feedCommentViewMoreCell"];
    
    //External Feed Cell
    UINib * externalFeedCell = [UINib nibWithNibName:@"PostTableViewCell" bundle:nil];
    [self.tableViewOutlet registerNib:externalFeedCell forCellReuseIdentifier:@"PostTableViewCell"];
    
}
-(void)viewWillAppear:(BOOL)animated{
    //[APPDELEGATE ShowHUDWith:@"Loading..."];
    
    strUserId=[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"];
    //[self callSinglePostData];
}
-(void)viewWillDisappear:(BOOL)animated{
    if (self.currentPlayedVideoCell) {
        [self makeItStahp:self.currentPlayedVideoCell];
    }
}
-(void)viewDidAppear:(BOOL)animated{
}
-(void)callSinglePostData
{
    //{"photo_id":"105","user_id":"42"}
    if (![self.postDataDetail objectForKey:@"photo_id"]) {
        
        _tableViewOutlet.hidden = YES;
        NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:
                            [self.postDataDetail objectForKey:@"item_id"],@"video_id",
                            strUserId,@"user_id",
                            self.typeItem, @"type",
                            nil];
        [[Connection sharedConnectionWithDelegate:self]getSinglePostData:dict];
    }
    else{
        _tableViewOutlet.hidden = YES;
        NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:
                            [self.postDataDetail objectForKey:@"photo_id"],@"photo_id",
                            strUserId,@"user_id",
                            self.typeItem, @"type",
                            nil];
        [[Connection sharedConnectionWithDelegate:self]getSinglePostData:dict];
    }
    
    
    
}
-(void)callCommentListDataForPhotoID:(NSString*)strPhotoID{
    //{"photo_id":"1"}
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",strPhotoID,@"photo_id", nil];
    [[Connection sharedConnectionWithDelegate:self]GetComment:dict];
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)moreBtnAction:(UIButton *)sender{
    //    self.sharingSheet= [[UIStoryboard defaultStoryboard] instantiateViewControllerWithIdentifier:@"SharingActionSheet"];
    //    self.sharingSheet.postData=self.postDataDetail;
    //    [[APPDELEGATE window] addSubview:self.sharingSheet.view];
    //    [self.sharingSheet presentActionSheet];
    
    
    int postUserId=[[self.postDataDetail objectForKey:@"user_id"] intValue];
    int userId=[[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"] intValue];
    
    if (postUserId==userId) {
        self.sharingSheet= [[UIStoryboard defaultStoryboard] instantiateViewControllerWithIdentifier:@"SharingActionSheet"];
        self.sharingSheet.postData=self.postDataDetail;
        self.sharingSheet.deleteBtnNeeded=YES;
        [[APPDELEGATE window] addSubview:self.sharingSheet.view];
        self.sharingSheet.delegate = self;
        [self.sharingSheet presentActionSheet];
    } else {
        self.sharingSheet= [[UIStoryboard defaultStoryboard] instantiateViewControllerWithIdentifier:@"SharingActionSheet"];
        self.sharingSheet.postData=self.postDataDetail;
        self.sharingSheet.deleteBtnNeeded=NO;
        [[APPDELEGATE window] addSubview:self.sharingSheet.view];
        self.sharingSheet.btnReportContent.intItemId = [[self.sharingSheet.postData valueForKey:@"photo_id"] integerValue];
        self.sharingSheet.btnReportContent.intUserId = postUserId;
        self.sharingSheet.delegate = self;
        [self.sharingSheet presentActionSheet];
    }
}



-(void)commentBtnAction:(UIButton *)sender{
    [self performSegueWithIdentifier:@"CommentSegue" sender:sender];
}
-(void)likeBtnAction:(UIButton *)sender{
    /*NSString *likeFlag;
    if (sender.tag==likeBtnTag) {
        likeFlag= @"1";
    }
    else if (sender.tag==disLikeBtnTag) {
        likeFlag= @"0";
    }
    // {"user_id":"","type_id":"photo","photo_id":"","like":"1"}
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    NSString *photoid=[self.postDataDetail objectForKey:@"photo_id"];
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:
                        strUserId,@"user_id",
                        photoid,@"photo_id",
                        @"photo",@"type_id",
                        likeFlag,@"like",
                        nil];
    [[Connection sharedConnectionWithDelegate:self] likeDislike:dict];*/
    
    
    NSString *likeFlag;
    if (sender.tag==likeBtnTag) {
        likeFlag= @"1";
    }
    else if (sender.tag==disLikeBtnTag) {
        likeFlag= @"0";
    }
    //CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblFeedList];
    //NSIndexPath *indexPath = [self.tblFeedList indexPathForRowAtPoint:buttonPosition];
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    
    NSDictionary *itemData = self.postDataDetail;
    NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithObjectsAndKeys:
                               [[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",
                               likeFlag,@"like",
                               nil];
    
    if ([itemData objectForKey:@"photo_id"]) {
        [dict setObject:[itemData objectForKey:@"photo_id"] forKey:@"photo_id"];
        [dict setObject:@"photo" forKey:@"type_id"];
    }
    
    if ([itemData objectForKey:@"video_id"]) {
        [dict setObject:[itemData objectForKey:@"video_id"] forKey:@"video_id"];
        [dict setObject:@"video" forKey:@"type_id"];
    }
    
    
    [[Connection sharedConnectionWithDelegate:self] likeDislike:dict];
    
    
}
-(void)userProfileNavigation:(UITapGestureRecognizer *)sender
{
    if ([[self.postDataDetail objectForKey:@"user_id"] isEqualToString:strUserId]) {
        [self.tabBarController setSelectedIndex:4];
        
        
        [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
        
    }else{
        [self performSegueWithIdentifier:@"SingleProfileVC" sender:sender];
    }
    
}

#pragma mark Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        _tableViewOutlet.hidden = NO;
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        if (strstatuscode == 200)
        {
            NSString *photoid = [self.postDataDetail objectForKey:@"photo_id"];
            if ([nState isEqualToString:STATELIKEDISLIKE]){
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    NSString *type_id = [dataDict objectForKey:@"type_id"];
                    NSString *itemID = nil;
                    NSString *itemName = @"";
                    if ([type_id isEqualToString:@"photo"]) {
                        itemID=[[[dataDict valueForKey:@"data"] objectAtIndex:0] objectForKey:@"photo_id"];
                        itemName = @"photo_id";
                    } else {
                        itemID=[[dataDict valueForKey:@"data"] objectForKey:@"video_id"];
                        itemName = @"video_id";
                    }
                    
                    if([[self.postDataDetail valueForKey:itemName] isEqualToString:itemID])
                    {
                        NSString *cur = [self.postDataDetail objectForKey:@"like"];
                        if ([cur isEqualToString:@"1"]) {
                            [self.postDataDetail setValue:@"0" forKey:@"like"];
                        } else {
                            [self.postDataDetail setValue:@"1" forKey:@"like"];
                        }
                        
                        NSString *curLike = [self.postDataDetail objectForKey:@"total_like"];
                        NSInteger curLikeInt = [curLike integerValue];
                        if ([[dataDict objectForKey:@"action"] isEqualToString:@"like"]) {
                            curLikeInt += 1;
                        } else {
                            curLikeInt = MAX(0, curLikeInt-1);
                        }
                        [self.postDataDetail setValue:[NSString stringWithFormat:@"%ld", (long)curLikeInt] forKey:@"total_like"];

                    }
                    
                    [self.tableViewOutlet reloadData];
                    [SVProgressHUD dismiss];
                }
                else{
                    [SVProgressHUD dismiss];
                }
            }
            else if([nState isEqualToString:STATESINGLEPOST]){
                if ([[dataDict valueForKey:@"IsSuccess"]boolValue]==YES) {
                    self.postDataDetail=[[dataDict objectForKey:@"data"] objectAtIndex:0];
                    [self.tableViewOutlet reloadData];
                    [SVProgressHUD dismiss];
                    
                    // Call comment
                    //[self callCommentListDataForPhotoID:photoid];
                }
                else{
                    //[UIAlertView showAlertViewWithTitle:ALERTTITLE message:[dataDict objectForKey:@"message"]];
                    [SVProgressHUD dismiss];
                }
            }
            
            // Comment Conditions
            if ([nState isEqualToString:STATEGETCOMMENT]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES ) {
                    self.commentArray = [NSMutableArray arrayWithArray:[dataDict objectForKey:@"data"]];
                } else {
                    [self.commentArray removeAllObjects];
                }
                NSLog(@"before reload: %@", NSStringFromCGRect(self.tableViewOutlet.frame));
                [self.tableViewOutlet reloadData];
                NSLog(@"AFTER reload: %@", NSStringFromCGRect(self.tableViewOutlet.frame));
                [SVProgressHUD dismiss];
            }
            else if ([nState isEqualToString:STATEADDCOMMENT]){
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    [self callCommentListDataForPhotoID:photoid];
                }
                else{
                    
                    [SVProgressHUD dismiss];
                }
            }
            else if ([nState isEqualToString:STATEDELETECOMMENT]){
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    [self.tableViewOutlet reloadData];
                    [self callCommentListDataForPhotoID:photoid];
                }
                else{
                    
                    [SVProgressHUD dismiss];
                }
            }
            
            
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
        [SVProgressHUD dismiss];
    }
}

- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
    _tableViewOutlet.hidden = NO;
    [SVProgressHUD dismiss];
    //    [UIAlertView showAlertViewWithTitle:ALERTTITLE message:nData];
}

#pragma mark - Actions
- (void)audioBtnAction:(UIButton*)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
    PostTableViewCell *cell = [self.tableViewOutlet cellForRowAtIndexPath:indexPath];
    
    cell.avPlayer.muted = ![cell.avPlayer isMuted];
    
    if (cell.avPlayer.muted) {
        [cell.audioButton setImage:[UIImage imageNamed:@"one-speaker-muted-icon.png"] forState:UIControlStateNormal];
    } else {
        [cell.audioButton setImage:[UIImage imageNamed:@"one-speaker-icon.png"] forState:UIControlStateNormal];
    }
}

- (void)showLikePopup:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
    
    LikedViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LikedViewController"];
    NSDictionary *itemData = self.postDataDetail;
    
    if ([[itemData objectForKey:@"total_like"] integerValue] == 0) {
        return;
    }
    
    if ([itemData objectForKey:@"photo_id"]) {
        vc.itemId = [itemData objectForKey:@"photo_id"];
        vc.typeId = @"photo";
    }
    
    if ([itemData objectForKey:@"video_id"]) {
        vc.itemId = [itemData objectForKey:@"video_id"];
        vc.typeId = @"video";
    }
    
    self.selectedPopupItemId = vc.itemId;
    self.selectedPopupTypeId = vc.typeId;
    
    UIView* contentView = vc.view;
    contentView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width*0.90, self.view.frame.size.height*0.25);
    [vc.collectionView setBackgroundColor:[UIColor clearColor]];
    [contentView setBackgroundColor:[UIColor clearColor]];
    
    UIView* popupView = [[UIView alloc] init];
    popupView.translatesAutoresizingMaskIntoConstraints = NO;
    popupView.layer.cornerRadius = 6.0;
    popupView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width*0.90, self.view.frame.size.height*0.30);
    [popupView setBackgroundColor:[UIColor whiteColor]];
    [popupView addSubview:contentView];
    
    UIButton *seeMoreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [seeMoreBtn setFrame:CGRectMake(0, popupView.frame.size.height-30, popupView.frame.size.width, 30)];
    [seeMoreBtn setImage:[UIImage imageNamed:@"social-dots"] forState:UIControlStateNormal];
    [seeMoreBtn addTarget:self action:@selector(likerPopupSeeMoreButtonDidPushed:) forControlEvents:UIControlEventTouchUpInside];
    [popupView addSubview:seeMoreBtn];
    
    [vc reinit];
    
    self.likerPopup = [KLCPopup popupWithContentView:popupView
                                            showType:KLCPopupShowTypeFadeIn
                                         dismissType:KLCPopupDismissTypeFadeOut
                                            maskType:KLCPopupMaskTypeDimmed
                            dismissOnBackgroundTouch:YES
                               dismissOnContentTouch:NO];
    [self.likerPopup show];
}

- (void)likerPopupSeeMoreButtonDidPushed:(UIButton*)sender
{
    if (self.likerPopup) {
        [self.likerPopup dismiss:YES];
        [self performSegueWithIdentifier:@"LikerListSegue" sender:self];
    }
}

- (void)playVideoButtonDidPushed:(UIButton*)sender
{
    
}

- (IBAction)boneButtonDidPushed:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableViewOutlet];
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
    PostTableViewCell *cell = [self.tableViewOutlet cellForRowAtIndexPath:indexPath];
    NSDictionary *itemData = self.postDataDetail;
    NSArray *tags = [itemData objectForKey:@"tag"];
    
    NSInteger maxTagUsername = 5;
    
    if (self.poptipArray == nil) {
        self.poptipArray = [[NSMutableArray alloc] init];
    }
    
    for (int i=0;i<tags.count;i++) {
        if (i==maxTagUsername) break;
        NSString *username = [[tags objectAtIndex:i] objectForKey:@"user_name"];
        CMPopTipView *tipView = [[CMPopTipView alloc] initWithMessage:username];
        tipView.backgroundColor = [UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0];
        tipView.has3DStyle = NO;
        tipView.delegate = self;
        tipView.textColor = [UIColor whiteColor];
        //tipView.dismissTapAnywhere = YES;
        tipView.triggerTapAnywhere = YES;
        tipView.hasGradientBackground = NO;
        tipView.textFont = [UIFont systemFontOfSize:12.0f];
        tipView.borderColor = [UIColor clearColor];
        
        [tipView presentPointingAtView:sender inView:cell.contentView animated:YES index:i];
        
        [self.poptipArray addObject:tipView];
    }
}

-(void)userPrfoileNavigation:(UITapGestureRecognizer *)sender
{
    [self performSegueWithIdentifier:@"SingleProfileVC" sender:nil];
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView              // Default is 1 if not implemented
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    NSInteger retVal = 2;
    if ([self.postDataDetail objectForKey:@"comments"] && [[self.postDataDetail objectForKey:@"comments"] count] > 0) {
        retVal = [[self.postDataDetail objectForKey:@"comments"] count] + 1;
    }
    return retVal;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 55;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    FeedHeaderView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"FeedHeaderView" owner:self options:nil] objectAtIndex:0];
    headerView.avatarImage.layer.borderWidth = 1.0f;
    headerView.avatarImage.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor];
    headerView.avatarImage.layer.masksToBounds = NO;
    headerView.avatarImage.clipsToBounds = YES;
    headerView.avatarImage.layer.cornerRadius = headerView.avatarImage.frame.size.width/2;
    NSString *strimageURL=[NSString stringWithFormat:@"%@",[self.postDataDetail objectForKey:@"user_image"]];
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [headerView.avatarImage setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic"] success:nil failure:nil];
    
    CGFloat usernameFontSize = 13.0f;
    CGFloat locationFontSize = 10.0f;
    if ([[self.postDataDetail objectForKey:@"location"] length]>0) {
        
        headerView.usernameLabel.frame = CGRectMake(45, 5, 150, 30);
        headerView.usernameLabel.text=[self.postDataDetail objectForKey:@"user_name"];
        headerView.usernameLabel.textColor=[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0];
        [headerView.usernameLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:usernameFontSize]];
        headerView.usernameLabel.userInteractionEnabled=YES;
        UITapGestureRecognizer *tapGest=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userPrfoileNavigation:)];
        [headerView.usernameLabel addGestureRecognizer:tapGest];
        
        UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(40, 30, 280, 15)];
        [btn setTitle:[self.postDataDetail objectForKey:@"location"] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageWithContentsOfBundleFileName:@"icn_location1"] forState:UIControlStateNormal];
        
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [btn addTarget:self action:@selector(locationBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]forState:UIControlStateNormal];
        [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:locationFontSize]];
        btn.tag=section;
        [headerView addSubview:btn];
        
        NSString *dateString = [self.postDataDetail objectForKey:@"time_stamp"];
        //headerView.timeLabel.text=[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:dateString]];
        [headerView.timeButton setTitle:[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:dateString]] forState:UIControlStateNormal];
        
        return headerView;
        
    } else {
        headerView.usernameLabel.frame = CGRectMake(45, 12, 150, 30);
        headerView.usernameLabel.text=[self.postDataDetail objectForKey:@"user_name"];
        headerView.usernameLabel.textColor=[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0];
        [headerView.usernameLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:usernameFontSize]];
        headerView.usernameLabel.userInteractionEnabled=YES;
        UITapGestureRecognizer *tapGest=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userPrfoileNavigation:)];
        [headerView.usernameLabel addGestureRecognizer:tapGest];
        
        NSString *dateString = [self.postDataDetail objectForKey:@"time_stamp"];
        //headerView.timeLabel.text=[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:dateString]];
        [headerView.timeButton setTitle:[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:dateString]] forState:UIControlStateNormal];
        
        return headerView;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row>0) {
        NSDictionary *commentData = [[self.postDataDetail objectForKey:@"comments"] objectAtIndex:indexPath.row-1];
        
        if ([commentData isKindOfClass:[NSString class]]) {
            return 44.0f;
        }
        
        NSData *strval = [Base64 decode:[commentData objectForKey:@"text"]];
        NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
        NSString *username = [commentData objectForKey:@"user_name"];
        NSString *wholeStr = [NSString stringWithFormat:@"%@ %@", username, strTextMsg];
        
        TTTAttributedLabel *tempLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(0, 0, 320, 72)];
        [self processLabelCommentWithCommentStr:wholeStr andUsername:username forLabel:tempLabel];
        
        return tempLabel.frame.size.height;
    } else {
        if (IS_IPHONE_5) {
            return 383.0f;
        } else if (IS_IPHONE_6){
            return 410.0f;
        } else {
            return 410.0f;
        }
    }
}
/*- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        return [self processPostCell:tableView cellForRowAtIndexPath:indexPath];
    } else {
        return [self processCommentCell:tableView cellForRowAtIndexPath:indexPath];
    }
    return nil;
}*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        return [self processFeedViewCellForTableView:tableView andIndexPath:indexPath];
    } else {
        if ([self.postDataDetail objectForKey:@"comments"] && [[self.postDataDetail objectForKey:@"comments"] count]) {
            return [self processCommentCellForTableView:tableView andIndexPath:indexPath];
        } else {
            return [self processFeedCommentEmptyCellForTableView:tableView andIndexPath:indexPath];
        }
    }
    return nil;
    
}

- (FeedCommentEmptyCell*)processFeedCommentEmptyCellForTableView:(UITableView *)tableView andIndexPath:(NSIndexPath*)indexPath
{
    FeedCommentEmptyCell *cell = (FeedCommentEmptyCell *)[tableView dequeueReusableCellWithIdentifier:@"FeedCommentEmptyCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (UITableViewCell*)processCommentCellForTableView:(UITableView *)tableView andIndexPath:(NSIndexPath*)indexPath
{
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell"];
    NSDictionary *commentData = [[self.postDataDetail objectForKey:@"comments"] objectAtIndex:indexPath.row-1];
    
    if ([commentData isKindOfClass:[NSString class]]) {
        FeedCommetViewMoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"feedCommentViewMoreCell"];
        cell.viewAllLabel.text = (NSString*)commentData;
        cell.item_id = [[self.postDataDetail objectForKey:@"photo_id"] integerValue];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else {
        NSData *strval = [Base64 decode:[commentData objectForKey:@"text"]];
        NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
        NSString *username = [commentData objectForKey:@"user_name"];
        NSString *wholeStr = [NSString stringWithFormat:@"%@ %@", username, strTextMsg];
        
        [self processLabelCommentWithCommentStr:wholeStr andUsername:username forLabel:cell.lblCommentText];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return nil;
}

- (UITableViewCell*)processCommentCell:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    SinglePostTableViewCell * cell  = [tableView dequeueReusableCellWithIdentifier:@"singleCommentCell" forIndexPath:indexPath];
    
    NSInteger index = indexPath.row - 1;
    NSDictionary *curD = [self.commentArray objectAtIndex:index];
    cell.lblUserName.text=[curD objectForKey:@"user_name"];
    //[cell.btnUserNameBtn addTarget:self action:@selector(UserNameBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSData *strval = [Base64 decode:[curD objectForKey:@"text"]];
    NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
    cell.lblCommentText.text=strTextMsg;

    cell.lblCommentTime.text=[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:[curD objectForKey:@"date"]]];
    
    cell.imgProfilePic.layer.borderWidth = 1.0f;
    cell.imgProfilePic.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor];
    cell.imgProfilePic.layer.masksToBounds = NO;
    cell.imgProfilePic.clipsToBounds = YES;
    cell.imgProfilePic.layer.cornerRadius = cell.imgProfilePic.frame.size.width/2;
    
    
    if ([[curD objectForKey:@"user_images"] length]>0) {
        
        NSString *strimageURL=[NSString stringWithFormat:@"%@",[curD objectForKey:@"user_images"]];
        
        NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
        [cell.imgProfilePic setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic"] success:nil failure:nil];
        
    }
    CGFloat fltMultiplyingFactor = [APPDELEGATE window].frame.size.width / 320;
    
    CGRect frame=cell.lblCommentText.frame;
    
    [cell.lblCommentText setNumberOfLines:0];
    
    cell.lblCommentText.lineBreakMode = NSLineBreakByTruncatingTail;
    CGFloat hight=[[curD objectForKey:@"text"] getHeightOfTextForFontSize:12.0 withLabelWidth:frame.size.width*fltMultiplyingFactor];
    [cell.lblCommentText setFrame:CGRectMake(cell.lblCommentText.frame.origin.x,
                                             cell.lblUserName.frame.size.height+18,
                                             frame.size.width,
                                             hight+3)];
    
    
    return cell;
}

- (UITableViewCell *)processPostCell:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    PostTableViewCell *cell = (PostTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"FeedViewCell" forIndexPath:indexPath];
    
    NSDictionary *dictData=self.postDataDetail;
    
    cell.lbluserName.text=[dictData objectForKey:@"user_name"];
    cell.btnComment.tag=indexPath.row;
    [cell.btnComment addTarget:self action:@selector(commentBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnLike.tag=indexPath.row;
    [cell.btnLike addTarget:self action:@selector(likeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    NSString *strimageURL=[NSString stringWithFormat:@"%@",[dictData objectForKey:@"post_image"]];
    
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [cell.imgPet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"image_626"] success:nil failure:nil];
    
    
    cell.lblStatus.text=[NSString stringWithFormat:@"%@",[dictData objectForKey:@"post_status"]];
    
    
    
    
    
    
    CGRect frame=cell.lblStatus.frame;
    [cell.lblStatus setNumberOfLines:0];
    CGFloat height=[[dictData objectForKey:@"post_status"] getHeightOfTextForFontSize:cell.lblStatus.font.pointSize withLabelWidth:frame.size.width];
    
    [cell.lblStatus setFrame:CGRectMake(cell.lblStatus.frame.origin.x,
                                        cell.lblStatus.frame.origin.y,
                                        frame.size.width,
                                        height+7)];
    
    
    cell.lblStatus.text=[NSString stringWithFormat:@"%@ %@",[dictData objectForKey:@"user_name"],[dictData objectForKey:@"post_status"]];
    
    NSData *strval = [Base64 decode:[dictData objectForKey:@"post_status"]];
    NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
    cell.lblStatus.text=[NSString stringWithFormat:@"%@ %@",[dictData objectForKey:@"user_name"],
                         [strTextMsg isEqualToString:@""]?[dictData objectForKey:@"post_status"]:strTextMsg];
    
    NSString *MainCommentString= cell.lblStatus.text;
    NSString *strUsername=[dictData objectForKey:@"user_name"];
    NSMutableAttributedString * string=[[NSMutableAttributedString alloc] initWithString:MainCommentString];
    NSRange rangeStatus=  [MainCommentString rangeOfString:MainCommentString];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:136.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0] range:rangeStatus];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:13.0f] range:rangeStatus];
    NSRange rangeUsername=  [MainCommentString rangeOfString:[NSString stringWithFormat:@"%@",strUsername]];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeUsername];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f] range:rangeUsername];
    for (int i=0; i<[[dictData objectForKey:@"tag_hash"] count]; i++)
    {
        NSString *strHashTag;
        NSString *strHash;
        
        if ([[dictData objectForKey:@"tag_hash"]count]>0) {
            strHashTag=[NSString stringWithFormat:@"#%@",[[dictData objectForKey:@"tag_hash"] objectAtIndex:i] ];
            strHash=[[dictData objectForKey:@"tag_hash"] objectAtIndex:i];
            
        }
        
        NSRange rangeHashTag=  [MainCommentString rangeOfString:[NSString stringWithFormat:@"%@",strHashTag]];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeHashTag];
        cell.lblStatus.delegate=self;
        cell.lblStatus.tag=1;
        NSArray *keys = [[NSArray alloc] initWithObjects:(id)kCTForegroundColorAttributeName,(id)kCTUnderlineStyleAttributeName
                         , nil];
        NSArray *objects = [[NSArray alloc] initWithObjects:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],[NSNumber numberWithInt:kCTUnderlineStyleNone], nil];
        NSDictionary *linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
        cell.lblStatus.linkAttributes = linkAttributes;
        [cell.lblStatus addLinkToPhoneNumber:strHash withRange:rangeHashTag];
    }
    
    cell.lblStatus.delegate=self;
    cell.lblStatus.tag=1;
    [cell.lblStatus addLinkToAddress:dictData withRange:rangeUsername];
    
    cell.lblStatus.attributedText=string;
    
    
    
    cell.lblLikeCount.text=[NSString stringWithFormat:@"%@",[dictData objectForKey:@"total_like"]];
    cell.lblCommentCount.text=[NSString stringWithFormat:@"%@",[dictData objectForKey:@"total_comment"]];
    
    if ([[dictData objectForKey:@"like"] boolValue]==YES) {
        [cell.btnLike setTag:likeBtnTag];
        [cell.btnLike setImage:nil forState:UIControlStateNormal];
        [cell.btnLike setImage:[UIImage imageWithContentsOfBundleFileName:@"dark_likeBox"] forState:UIControlStateNormal];
    }
    else{
        [cell.btnLike setTag:disLikeBtnTag];
        [cell.btnLike setImage:nil forState:UIControlStateNormal];
        [cell.btnLike setImage:[UIImage imageWithContentsOfBundleFileName:@"light_likeBox"] forState:UIControlStateNormal];
    }
    //cell.lblTime.text=[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:[dictData objectForKey:@"date"]]];
    cell.btnMore.tag=indexPath.row;
    [cell.btnMore addTarget:self action:@selector(moreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    //    NSDate *object = _objects[indexPath.row];
    
    
    if ([[dictData objectForKey:@"tag"] count]==0)
    {
        cell.lblTaggedUsers.hidden=YES;
        cell.lblStatus.frame=CGRectMake(cell.lblStatus.frame.origin.x,cell.operationView.frame.origin.y+cell.operationView.frame.size.height,  cell.lblStatus.frame.size.width,  cell.lblStatus.frame.size.height);
        
    }
    else
    {
        
        cell.lblTaggedUsers.hidden=NO;
        
        NSString *strUserName=[[[dictData objectForKey:@"tag"] objectAtIndex:0]objectForKey:@"user_name"];
        
        
        cell.lblStatus.frame=CGRectMake(cell.lblStatus.frame.origin.x,cell.lblTaggedUsers.frame.origin.y+cell.lblTaggedUsers.frame.size.height,  cell.lblStatus.frame.size.width,  cell.lblStatus.frame.size.height);
        
        if ([[dictData objectForKey:@"tag"] count]==1) {
            cell.lblTaggedUsers.text=[NSString stringWithFormat:@"%@ is tagged in this post",[[[dictData objectForKey:@"tag"] objectAtIndex:0]objectForKey:@"user_name"] ];
            
            
            
            
            if ([[dictData objectForKey:@"tag"] count]==1)
            {
                cell.lblTaggedUsers.text=[NSString stringWithFormat:@"%@ is tagged",strUserName];
                
                
                NSString *MainPostString=[NSString stringWithFormat:@"%@ is tagged",strUserName];
                
                
                NSMutableAttributedString * string=[[NSMutableAttributedString alloc] initWithString:MainPostString];
                
                
                NSRange rangeUsername=  [MainPostString rangeOfString:[NSString stringWithFormat:@"%@",strUserName]];
                
                
                NSRange rangeIsTagged=  [MainPostString rangeOfString:[NSString stringWithFormat:@" is tagged"]];
                
                
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:136.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0] range:rangeIsTagged];
                [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:13.0f] range:rangeIsTagged];
                
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeUsername];
                [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f] range:rangeUsername];
                
                
                cell.lblTaggedUsers.delegate=self;
                cell.lblTaggedUsers.tag=2;
                
                
                NSArray *keys = [[NSArray alloc] initWithObjects:(id)kCTForegroundColorAttributeName,(id)kCTUnderlineStyleAttributeName
                                 , nil];
                
                NSArray *objects = [[NSArray alloc] initWithObjects:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],[NSNumber numberWithInt:kCTUnderlineStyleNone], nil];
                NSDictionary *linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
                
                cell.lblTaggedUsers.linkAttributes = linkAttributes;
                [cell.lblTaggedUsers addLinkToAddress:dictData withRange:rangeUsername];
                
                cell.lblTaggedUsers.attributedText=string;
                
            }
            
            
            
        }
        else{
            cell.lblTaggedUsers.text=[NSString stringWithFormat:@"%@ and %u more are tagged in this post",[[[dictData objectForKey:@"tag"] objectAtIndex:0]objectForKey:@"user_name"],[[dictData objectForKey:@"tag"]count]-1];
            
            cell.lblTaggedUsers.text=[NSString stringWithFormat:@"%@ and %u more are tagged",strUserName,[[dictData objectForKey:@"tag"]count]-1];
            
            NSString *MainPostString=[NSString stringWithFormat:@"%@ and %u more are tagged",strUserName,[[dictData objectForKey:@"tag"]count]-1];
            
            
            NSMutableAttributedString * string=[[NSMutableAttributedString alloc] initWithString:MainPostString];
            
            
            NSRange rangeUsername=  [MainPostString rangeOfString:[NSString stringWithFormat:@"%@",strUserName]];
            NSRange rangeNumberofTaggedUsers=  [MainPostString rangeOfString:[NSString stringWithFormat:@"%u more",[[dictData objectForKey:@"tag"]count]-1]];
            
            NSRange rangeAnd=  [MainPostString rangeOfString:[NSString stringWithFormat:@" and "]];
            NSRange rangeAreTagged=  [MainPostString rangeOfString:[NSString stringWithFormat:@" are tagged"]];
            
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:136.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0] range:rangeAnd];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:13.0f] range:rangeAnd];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:136.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0] range:rangeAreTagged];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:13.0f] range:rangeAreTagged];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeUsername];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f] range:rangeUsername];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeNumberofTaggedUsers];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f] range:rangeNumberofTaggedUsers];
            
            
            cell.lblTaggedUsers.delegate=self;
            cell.lblTaggedUsers.tag=2;
            
            NSArray *keys = [[NSArray alloc] initWithObjects:(id)kCTForegroundColorAttributeName,(id)kCTUnderlineStyleAttributeName
                             , nil];
            
            NSArray *objects = [[NSArray alloc] initWithObjects:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],[NSNumber numberWithInt:kCTUnderlineStyleNone], nil];
            NSDictionary *linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
            
            cell.lblTaggedUsers.linkAttributes = linkAttributes;
            [cell.lblTaggedUsers addLinkToPhoneNumber:nil withRange:rangeNumberofTaggedUsers];
            
            [cell.lblTaggedUsers addLinkToAddress:dictData withRange:rangeUsername];
            
            
            cell.lblTaggedUsers.attributedText=string;
        }
        
    }
    
    {
        cell.imgPet.frame = CGRectMake(cell.imgPet.frame.origin.x, cell.imgPet.frame.origin.y, cell.imgPet.frame.size.width, cell.imgPet.frame.size.width);
        cell.operationView.frame = CGRectMake(cell.operationView.frame.origin.x, cell.imgPet.frame.origin.y + cell.imgPet.frame.size.height + 3, cell.operationView.frame.size.width, cell.operationView.frame.size.height);
        
        if (!cell.lblTaggedUsers.hidden)
        {
            cell.lblTaggedUsers.frame = CGRectMake(cell.lblTaggedUsers.frame.origin.x, cell.operationView.frame.origin.y + cell.operationView.frame.size.height, cell.lblTaggedUsers.frame.size.width, cell.lblTaggedUsers.frame.size.height);
            cell.lblStatus.frame = CGRectMake(cell.lblStatus.frame.origin.x, cell.lblTaggedUsers.frame.origin.y + cell.lblTaggedUsers.frame.size.height, cell.lblStatus.frame.size.width, cell.lblStatus.frame.size.height);
        }
        else
        {
            cell.lblStatus.frame = CGRectMake(cell.lblStatus.frame.origin.x, cell.operationView.frame.origin.y + cell.operationView.frame.size.height, cell.lblStatus.frame.size.width, cell.lblStatus.frame.size.height);
        }
    }
    
    
    return cell;
}



- (void)processLabelCommentWithCommentStr:(NSString*)wholeStr andUsername:(NSString*)username forLabel:(TTTAttributedLabel*)label
{
    NSMutableAttributedString *attri_str=[[NSMutableAttributedString alloc]initWithString:wholeStr];
    NSInteger begin= 0;
    NSInteger end=[username length];
    [attri_str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:kCOMMENT_FONT_SIZE] range:NSMakeRange(begin, end)];
    [attri_str addAttribute:NSForegroundColorAttributeName value:kDEFAULT_BLUE_COLOR range:NSMakeRange(begin, end)];
    [label setNumberOfLines:0];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.text=attri_str;
    
    [label setTextColor:[UIColor grayColor]];
    
    //Search hashtag
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"#(\\w+)" options:0 error:&error];
    NSArray *matches = [regex matchesInString:[attri_str string] options:0 range:NSMakeRange(0, [attri_str string].length)];
    for (NSTextCheckingResult *match in matches) {
        NSRange wordRange = [match rangeAtIndex:1];
        NSString* word = [[attri_str string] substringWithRange:wordRange];
        NSRange editedRange = NSMakeRange(wordRange.location-1, wordRange.length+1);
        
        label.delegate=self;
        label.tag=99;
        
        NSArray *keys = [[NSArray alloc] initWithObjects:(id)kCTForegroundColorAttributeName,(id)kCTUnderlineStyleAttributeName, nil];
        NSArray *objects = [[NSArray alloc] initWithObjects:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],[NSNumber numberWithInt:kCTUnderlineStyleNone], nil];
        NSDictionary *linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
        label.linkAttributes = linkAttributes;
        
        [label addLinkToURL:[NSURL URLWithString:[NSString stringWithFormat:@"hashtag:%@", word]] withRange:editedRange];
    }
    
    
    CGFloat fltMultiplyingFactor = [APPDELEGATE window].frame.size.width / 320;
    
    CGRect frame=label.frame;
    //CGFloat hight=[[attri_str string] getHeightOfTextForFontSize:12.0 withLabelWidth:frame.size.width*fltMultiplyingFactor];
    CGFloat height = [self calculateLabelFrameForText:[attri_str string] andLabelWidth:frame.size.width*fltMultiplyingFactor];
    [label setFrame:CGRectMake(label.frame.origin.x,
                               0,
                               frame.size.width,
                               MAX(21.0f, height))];
    
    //NSLog(@"Comment: %@ --- frame: %@", [attri_str string], NSStringFromCGRect(label.frame));
}

- (CGFloat)calculateLabelFrameForText:(NSString*)str andLabelWidth:(CGFloat)labelWidth
{
    CGFloat height=[str getHeightOfTextForFontSize:kCOMMENT_FONT_SIZE withLabelWidth:labelWidth];
    return MAX(height, 21.0f);
}

- (PostTableViewCell*)processFeedViewCellForTableView:(UITableView *)tableView andIndexPath:(NSIndexPath*)indexPath
{
    PostTableViewCell *cell = (PostTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"PostTableViewCell" forIndexPath:indexPath];
    //FeedViewCell *cell = (FeedViewCell *)[tableView dequeueReusableCellWithIdentifier:@"FeedViewCell" forIndexPath:indexPath];
    cell.indexPath = indexPath;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell.activityIndicator setHidden:YES];
    
    
    cell.lbluserName.text=[self.postDataDetail objectForKey:@"user_name"];
    cell.btnComment.tag=indexPath.row;
    [cell.btnComment addTarget:self action:@selector(commentBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnComment2 addTarget:self action:@selector(commentBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.audioButton addTarget:self action:@selector(audioBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnBoneTag addTarget:self action:@selector(boneButtonDidPushed:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnLike.tag=indexPath.row;
    [cell.btnLike setTitle:[NSString stringWithFormat:@"%@ Likes",[self.postDataDetail objectForKey:@"total_like"]] forState:UIControlStateNormal];
    [cell.btnLike addTarget:self action:@selector(showLikePopup:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnLikeCenter addTarget:self action:@selector(likeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.playVideoButton addTarget:self action:@selector(playVideoButtonDidPushed:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnMore.tag=indexPath.row;
    [cell.btnMore addTarget:self action:@selector(moreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *strimageURL=[NSString stringWithFormat:@"%@",[self.postDataDetail objectForKey:@"post_image"]];
    
    //for caching image is necessory then use this.
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL]
                                                                     cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [cell.imgPet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"image_626"] success:nil failure:nil];
    
    [cell.playVideoButton setHidden:YES];
    [cell.moviePlayer.view setHidden:YES];
    
    [cell.imgPet setHidden:NO];
    [cell.moviePlayer.view setHidden:YES];
    [cell.playVideoButton setHidden:YES];
    [cell.audioButton setHidden:YES];
    
    //if (self.fullVisible && !self.isScrolling) {
        if ([self.postDataDetail objectForKey:@"video_url"]) {
            NSData *strval = [Base64 decode:[self.postDataDetail objectForKey:@"post_status"]];
            NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
            
            NSURL *videoURL = [NSURL URLWithString:[self.postDataDetail objectForKey:@"video_url"]];
            //NSURL *videoURL = [[NSBundle mainBundle] URLForResource:@"Kunto Aji - Terlalu Lama Sendiri" withExtension:@"mp4"];
            //NSURL *videoURL = [NSURL URLWithString:@"http://nordenmovil.com/urrea/InstalaciondelavaboURREAbaja.mp4"];
            
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
            
            dispatch_async(queue, ^{
                cell.avPlayerItem = [AVPlayerItem playerItemWithURL:videoURL];
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    cell.avPlayer = [AVPlayer playerWithPlayerItem:cell.avPlayerItem];
                    cell.avLayer = [AVPlayerLayer playerLayerWithPlayer:cell.avPlayer];
                    cell.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
                    
                    [self makeItPlay:cell];
                });
            });
            
        }
//    } else {
//        NSLog(@"cell fullVisible: %@", (self.fullVisible)?@"YES":@"NO");
//        [self makeItStahp:cell];
//    }
    
    cell.lblLikeCount.text=[NSString stringWithFormat:@"%@",[self.postDataDetail objectForKey:@"total_like"]];
    cell.lblCommentCount.text=[NSString stringWithFormat:@"%@",[self.postDataDetail objectForKey:@"total_comment"]];
    
    if ([[self.postDataDetail objectForKey:@"like"] boolValue]==YES) {
        [cell.btnLike setTag:likeBtnTag];
        [cell.btnLikeCenter setTag:likeBtnTag];
        [cell.btnLikeCenter setImage:[UIImage imageNamed:@"center-unlike-button2.png"] forState:UIControlStateNormal];
    } else {
        [cell.btnLike setTag:disLikeBtnTag];
        [cell.btnLikeCenter setTag:disLikeBtnTag];
        [cell.btnLikeCenter setImage:[UIImage imageNamed:@"center-like-button.png"] forState:UIControlStateNormal];
    }
    
    //cell.lblTime.text=[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:[self.postDataDetail objectForKey:@"date"]]];

    CGRect frame = cell.lblStatus.frame;
    [cell.lblStatus setNumberOfLines:0];
    [cell.lblStatus setLineBreakMode:NSLineBreakByWordWrapping];
    
    
    CGFloat height=[[self.postDataDetail objectForKey:@"post_status"] getHeightOfTextForFontSize:cell.lblStatus.font.pointSize withLabelWidth:frame.size.width];
    [cell.lblStatus setFrame:CGRectMake(cell.lblStatus.frame.origin.x,
                                        cell.lblStatus.frame.origin.y,
                                        frame.size.width,
                                        height+7)];
    
    if ([[self.postDataDetail objectForKey:@"tag"] count]==0)
    {
        cell.lblTaggedUsers.hidden=YES;
        cell.lblStatus.frame=CGRectMake(cell.lblStatus.frame.origin.x,cell.operationView.frame.origin.y+cell.operationView.frame.size.height,  cell.lblStatus.frame.size.width,  cell.lblStatus.frame.size.height);
    }
    else
    {
        cell.lblTaggedUsers.hidden=NO;//cell.btnLike.frame.origin.y+cell.btnLike.frame.size.height
        cell.lblTaggedUsers.hidden=YES;
        cell.lblStatus.frame=CGRectMake(cell.lblStatus.frame.origin.x,cell.lblTaggedUsers.frame.origin.y+cell.lblTaggedUsers.frame.size.height,  cell.lblStatus.frame.size.width,  cell.lblStatus.frame.size.height);
        
        NSString *strUserName=[[[self.postDataDetail objectForKey:@"tag"] objectAtIndex:0]objectForKey:@"user_name"];
        
        
        if ([[self.postDataDetail objectForKey:@"tag"] count]==1) {
            cell.lblTaggedUsers.text=[NSString stringWithFormat:@"%@ is tagged",strUserName];
            
            
            NSString *MainPostString=[NSString stringWithFormat:@"%@ is tagged",strUserName];
            
            
            NSMutableAttributedString * string=[[NSMutableAttributedString alloc] initWithString:MainPostString];
            
            
            NSRange rangeUsername=  [MainPostString rangeOfString:[NSString stringWithFormat:@"%@",strUserName]];
            
            
            NSRange rangeIsTagged=  [MainPostString rangeOfString:[NSString stringWithFormat:@" is tagged"]];
            
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:136.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0] range:rangeIsTagged];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:13.0f] range:rangeIsTagged];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeUsername];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f] range:rangeUsername];
            
            cell.lblTaggedUsers.delegate=self;
            cell.lblTaggedUsers.tag=2;
            
            NSArray *keys = [[NSArray alloc] initWithObjects:(id)kCTForegroundColorAttributeName,(id)kCTUnderlineStyleAttributeName
                             , nil];
            
            NSArray *objects = [[NSArray alloc] initWithObjects:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],[NSNumber numberWithInt:kCTUnderlineStyleNone], nil];
            NSDictionary *linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
            
            cell.lblTaggedUsers.linkAttributes = linkAttributes;
            [cell.lblTaggedUsers addLinkToAddress:self.postDataDetail withRange:rangeUsername];
            
            cell.lblTaggedUsers.attributedText=string;
        } else {
            cell.lblTaggedUsers.text=[NSString stringWithFormat:@"%@ and %lu more are tagged",strUserName,[[self.postDataDetail objectForKey:@"tag"]count]-1];
            
            NSString *MainPostString=[NSString stringWithFormat:@"%@ and %lu more are tagged",strUserName,[[self.postDataDetail objectForKey:@"tag"]count]-1];
            
            
            NSMutableAttributedString * string=[[NSMutableAttributedString alloc] initWithString:MainPostString];
            
            NSRange rangeUsername=  [MainPostString rangeOfString:[NSString stringWithFormat:@"%@",strUserName]];
            NSRange rangeNumberofTaggedUsers=  [MainPostString rangeOfString:[NSString stringWithFormat:@"%lu more",[[self.postDataDetail objectForKey:@"tag"]count]-1]];
            
            NSRange rangeAnd=  [MainPostString rangeOfString:[NSString stringWithFormat:@" and "]];
            NSRange rangeAreTagged=  [MainPostString rangeOfString:[NSString stringWithFormat:@" are tagged"]];
            
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:136.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0] range:rangeAnd];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:13.0f] range:rangeAnd];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:136.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0] range:rangeAreTagged];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:13.0f] range:rangeAreTagged];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeUsername];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f] range:rangeUsername];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0] range:rangeNumberofTaggedUsers];
            [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f] range:rangeNumberofTaggedUsers];
            cell.lblTaggedUsers.delegate=self;
            cell.lblTaggedUsers.tag=2;
            NSArray *keys = [[NSArray alloc] initWithObjects:(id)kCTForegroundColorAttributeName,(id)kCTUnderlineStyleAttributeName
                             , nil];
            NSArray *objects = [[NSArray alloc] initWithObjects:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],[NSNumber numberWithInt:kCTUnderlineStyleNone], nil];
            NSDictionary *linkAttributes = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
            cell.lblTaggedUsers.linkAttributes = linkAttributes;
            [cell.lblTaggedUsers addLinkToPhoneNumber:nil withRange:rangeNumberofTaggedUsers];
            [cell.lblTaggedUsers addLinkToAddress:self.postDataDetail withRange:rangeUsername];
            cell.lblTaggedUsers.attributedText=string;
        }
    }
    
    
    cell.imgPet.frame = CGRectMake(cell.imgPet.frame.origin.x, cell.imgPet.frame.origin.y, cell.imgPet.frame.size.width, cell.imgPet.frame.size.width);
    cell.operationView.frame = CGRectMake(cell.operationView.frame.origin.x, cell.imgPet.frame.origin.y + cell.imgPet.frame.size.height + 3, cell.operationView.frame.size.width, cell.operationView.frame.size.height);
    
    if (!cell.lblTaggedUsers.hidden)
    {
        cell.lblTaggedUsers.frame = CGRectMake(cell.lblTaggedUsers.frame.origin.x, cell.operationView.frame.origin.y + cell.operationView.frame.size.height, cell.lblTaggedUsers.frame.size.width, cell.lblTaggedUsers.frame.size.height);
        cell.lblStatus.frame = CGRectMake(cell.lblStatus.frame.origin.x, cell.lblTaggedUsers.frame.origin.y + cell.lblTaggedUsers.frame.size.height, cell.lblStatus.frame.size.width, cell.lblStatus.frame.size.height);
    }
    else
    {
        cell.lblStatus.frame = CGRectMake(cell.lblStatus.frame.origin.x, cell.operationView.frame.origin.y + cell.operationView.frame.size.height, cell.lblStatus.frame.size.width, cell.lblStatus.frame.size.height);
    }
    return cell;
}


- (void)makeItPlay:(PostTableViewCell*)cell
{
    [cell.imgPet setHidden:YES];
    [cell.activityIndicator setHidden:NO];
    AVAsset *currentPlayerAsset = cell.avPlayer.currentItem.asset;
    [cell.audioButton setHidden:NO];
    
    NSLog(@"Prepare to play: %@", [(AVURLAsset *)currentPlayerAsset URL]);
    cell.avLayer.frame = cell.imgPet.frame;
    [cell.contentView.layer addSublayer:cell.avLayer];
    cell.avPlayer.muted = YES;
    [cell.audioButton setImage:[UIImage imageNamed:@"one-speaker-muted-icon.png"] forState:UIControlStateNormal];
    [cell.avPlayer play];
    
}

- (void)makeItStahp:(PostTableViewCell*)cell
{
    [cell.moviePlayer.view setHidden:YES];
    [cell.activityIndicator setHidden:YES];
    [cell.audioButton setHidden:YES];
    [cell.imgPet setHidden:NO];
    [cell.avPlayer pause];
    [cell.avLayer removeFromSuperlayer];
    cell.avPlayer = nil;
    cell.avPlayerItem = nil;
    
    [cell.audioButton setImage:[UIImage imageNamed:@"one-speaker-muted-icon.png"] forState:UIControlStateNormal];
}


#pragma mark - Delegate Methods of TTTAttributedLabel -


- (void)attributedLabel:(TTTAttributedLabel *)label
didSelectLinkWithPhoneNumber:(NSString *)strHashTag{
    
    if (label.tag==1){
        HashTagViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HashTagViewController"];
        vc.strHashTag=strHashTag;
        [self.navigationController pushViewController:vc animated:YES];
        
        //        [self performSegueWithIdentifier:@"HashTagSegue" sender:self];
    }else if(label.tag==2){
        CGPoint buttonPosition = [label convertPoint:CGPointZero toView:self.tableViewOutlet];
        NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
        [self performSegueWithIdentifier:@"TaggedUserSegue" sender:indexPath];
    }
}
-(IBAction)commentToFeed:(UIStoryboardSegue *)segue{
    if ([segue.sourceViewController isKindOfClass:[CommentVC class]]) {
        CommentVC *commentVC = segue.sourceViewController;
    }
}
- (void)attributedLabel:(TTTAttributedLabel *)label
didSelectLinkWithAddress:(NSDictionary *)addressComponents
{
    
    if (label.tag==1) {
        CGPoint buttonPosition = [label convertPoint:CGPointZero toView:self.tableViewOutlet];
        NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
        if ([[addressComponents objectForKey:@"user_id"] isEqualToString:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"]]) {
            [self.tabBarController setSelectedIndex:4];
            [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
            
        }else{
            
            [self performSegueWithIdentifier:@"SingleProfileVC" sender:indexPath];
        }
    }
    else if(label.tag==2) {
        CGPoint buttonPosition = [label convertPoint:CGPointZero toView:self.tableViewOutlet];
        NSIndexPath *indexPath = [self.tableViewOutlet indexPathForRowAtPoint:buttonPosition];
        
        if ([[[[self.postDataDetail objectForKey:@"tag"] objectAtIndex:0] objectForKey:@"user_id"] isEqualToString:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"]]) {
            [self.tabBarController setSelectedIndex:4];
            
            [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
            
        }else{
            [self performSegueWithIdentifier:@"TaggedUser" sender:indexPath];
        }
    }
    
    
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"CommentSegue"]) {
        
        CommentVC *commentVC=(CommentVC *)[segue destinationViewController];
        commentVC.strPhotoID=[self.postDataDetail objectForKey:@"photo_id"];
        
    }
    else if ([segue.identifier isEqualToString:@"SingleProfileVC"]){
        UserProfileVC *userProfile=(UserProfileVC *)[segue destinationViewController];
        userProfile.userDetails=self.postDataDetail;
    }
    else if([segue.identifier isEqualToString:@"TaggedUser"]){
        UserProfileVC *userProfile=(UserProfileVC *)[segue destinationViewController];
        userProfile.userDetails=[[self.postDataDetail objectForKey:@"tag"] objectAtIndex:0];
    }
    else if([segue.identifier isEqualToString:@"TaggedUserSegue"]){
        TaggedUsersViewController *tagUsers=(TaggedUsersViewController *)[segue destinationViewController];
        tagUsers.arrayData=[_postDataDetail objectForKey:@"tag"];
    } else if ([segue.identifier isEqualToString:@"reportVC"]) {
        ReportVC *rVC = [segue destinationViewController];
        rVC.intItemId = [[self.sharingSheet.postData valueForKey:@"photo_id"] integerValue];
        rVC.intUserId = [[self.postDataDetail objectForKey:@"user_id"] intValue];
    } else if ([segue.identifier isEqualToString:@"LikerListSegue"]) {
        LikerListViewController *lkvc = (LikerListViewController*)[segue destinationViewController];
        lkvc.itemId = self.selectedPopupItemId;
        lkvc.typeId = self.selectedPopupTypeId;
    }
}


- (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SharingActionSheet Delegate Methods
- (void)reportBtnDidPushed
{
    [self performSegueWithIdentifier:@"reportVC" sender:self];
}

#pragma mark - TipPopup Delegate Methods
- (void)triggerAnywhereFired:(CMPopTipView *)popTipView
{
    [self eraseAllPoptip];
}

- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
    
}

- (void)eraseAllPoptip
{
    if (self.poptipArray) {
        for (CMPopTipView *obj in self.poptipArray) {
            [obj dismissAnimated:YES];
        }
        self.poptipArray = nil;
    }
}

@end
