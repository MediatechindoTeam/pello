//
//  CommentVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/27/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "CommentVC.h"
#import "CommentCell.h"
#import "UserProfileVC.h"
#define kOFFSET_FOR_KEYBOARD 166.0

@interface CommentVC ()
{
    NSMutableArray*dataArray;
}
@end

@implementation CommentVC
@synthesize txtAddComment,viewAddComment,CommentTableViewOutlet,strPhotoID;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CommentTableViewOutlet.hidden = YES;
    
    // Do any additional setup after loading the view.
    [self preferredStatusBarStyle];
    [self callCommentListData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    messageLabel.hidden = YES;
    if (messageLabel==nil) {
        messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        messageLabel.hidden=YES;
    }
    txtAddComment.autocorrectionType=UITextAutocorrectionTypeNo;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
     [self setTextFieldPadding:txtAddComment];
    
}
- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    int MINheight = MIN(keyboardSize.height,keyboardSize.width);
    int MINwidth = MIN(keyboardSize.height,keyboardSize.width);
    
    int MAXheight = MAX(keyboardSize.height,keyboardSize.width);
    int MAXwidth = MAX(keyboardSize.height,keyboardSize.width);
    
     [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    self.viewAddComment.frame=CGRectMake(0, self.view.bounds.size.height - self.viewAddComment.bounds.size.height-keyboardSize.height,self.viewAddComment.frame.size.width, self.viewAddComment.frame.size.height);
    CGRect frame = self.viewAddComment.frame;
    
    [UIView commitAnimations];


    //your other code here..........
}
-(void)viewWillDisappear:(BOOL)animated
{
    //  [self.navigationController setNavigationBarHidden:YES animated:YES];
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    
    
    
    [super viewWillDisappear:YES];
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)callCommentListData{
    
    
    //{"photo_id":"1"}
    [APPDELEGATE ShowHUDWith:@"Loading..."];
     NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",strPhotoID,@"photo_id", nil];
    [[Connection sharedConnectionWithDelegate:self]GetComment:dict];
}

#pragma mark - Keyboard Hide show methods -
-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (self.viewAddComment.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.viewAddComment.frame.origin.y < 0)
    {
     }
}

-(void)keyboardWillHide {
    [self setViewMovedUp:NO];
}
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.viewAddComment.frame;
    if (movedUp)
    {
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
       
        if ([self.CommentTableViewOutlet numberOfSections]) {
            int lastRowNumber = (int)[self.CommentTableViewOutlet numberOfRowsInSection:0] - 1;
            if (lastRowNumber>0) {
                NSIndexPath* ip = [NSIndexPath indexPathForRow:lastRowNumber inSection:0];
                [self.CommentTableViewOutlet scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:NO];
               
            }
            self.viewAddComment.frame = rect;

        }
         self.viewAddComment.frame = rect;
    }
    else
    {

        if ([self.CommentTableViewOutlet numberOfSections]) {
            int lastRowNumber = (int) [self.CommentTableViewOutlet numberOfRowsInSection:0] - 1;
            if (lastRowNumber>0) {
                NSIndexPath* ip = [NSIndexPath indexPathForRow:lastRowNumber inSection:0];
                [self.CommentTableViewOutlet scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:NO];
            }
           
            

        }
         rect.origin.y += kOFFSET_FOR_KEYBOARD;
        self.viewAddComment.frame = rect;
    }
    
    
    [UIView commitAnimations];
}


- (IBAction)backBtnAction:(UIButton *)sender {
    self.strTotalComment=[NSString stringWithFormat:@"%d",dataArray.count];
 }
#pragma mark - TableView Datasource Delegate -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    // Return the number of sections.
    if ([dataArray count]!=0) {
        messageLabel.hidden=YES;
        return 1;
    }
    else
    {
        // Display a message when the table is empty
        messageLabel.hidden=NO;
        messageLabel.text = @"No comments yet.Be the first to comment.";
        messageLabel.textColor = [UIColor grayColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
        [messageLabel sizeToFit];
        CommentTableViewOutlet.backgroundView = messageLabel;
        CommentTableViewOutlet.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 0;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}
-(void)UserNameBtnAction:(UIButton *)sender
{
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:CommentTableViewOutlet];
        NSIndexPath *indexPath = [CommentTableViewOutlet indexPathForRowAtPoint:buttonPosition];
        if ([[[dataArray objectAtIndex:indexPath.row] objectForKey:@"user_id"] isEqualToString:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"]]) {
            [self.tabBarController setSelectedIndex:4];
            [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
            
        }else{
            
            [self performSegueWithIdentifier:@"SingleProfileVC" sender:indexPath];
        }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
     if ([segue.identifier isEqualToString:@"SingleProfileVC"]){
        UserProfileVC *userProfile=(UserProfileVC *)[segue destinationViewController];
         NSIndexPath *indexPath=(NSIndexPath *)sender;
        userProfile.userDetails=[dataArray objectAtIndex:indexPath.row];
    }
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID =  @"CommentCell";
    
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        cell = [[CommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.lblUserName.text=[[dataArray objectAtIndex:indexPath.row] objectForKey:@"user_name"];
    [cell.btnUserNameBtn addTarget:self action:@selector(UserNameBtnAction:) forControlEvents:UIControlEventTouchUpInside];

    
   // cell.lblCommentText.text=[[dataArray objectAtIndex:indexPath.row] objectForKey:@"text"];
    
    NSData *strval = [Base64 decode:[[dataArray objectAtIndex:indexPath.row] objectForKey:@"text"]];
    NSString *strTextMsg = [[NSString alloc] initWithData:strval encoding:NSUTF8StringEncoding];
    cell.lblCommentText.text=strTextMsg;
    
    
  cell.lblCommentTime.text=[Constant durationbetweendate:[Constant UTCtoDeviceTimeZone:[[dataArray objectAtIndex:indexPath.row] objectForKey:@"date"]]];

    cell.imgProfilePic.layer.borderWidth = 1.0f;
    cell.imgProfilePic.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor];
    cell.imgProfilePic.layer.masksToBounds = NO;
    cell.imgProfilePic.clipsToBounds = YES;
    cell.imgProfilePic.layer.cornerRadius = cell.imgProfilePic.frame.size.width/2;
    
    
    if ([[[dataArray objectAtIndex:indexPath.row] objectForKey:@"user_images"] length]>0) {

        NSString *strimageURL=[NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"user_images"]];

        NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
        [cell.imgProfilePic setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic"] success:nil failure:nil];

    }
    CGFloat fltMultiplyingFactor = [APPDELEGATE window].frame.size.width / 320;

    CGRect frame=cell.lblCommentText.frame;

    [cell.lblCommentText setNumberOfLines:0];

    cell.lblCommentText.lineBreakMode = NSLineBreakByTruncatingTail;
        CGFloat hight=[[[dataArray objectAtIndex:indexPath.row] objectForKey:@"text"] getHeightOfTextForFontSize:12.0 withLabelWidth:frame.size.width*fltMultiplyingFactor];
    [cell.lblCommentText setFrame:CGRectMake(cell.lblCommentText.frame.origin.x,
                                                 cell.lblUserName.frame.size.height+18,
                                                 frame.size.width,
                                                 hight+3)];

    
     return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat fltMultiplyingFactor = [APPDELEGATE window].frame.size.width / 320;
    CGFloat hight=[[[dataArray objectAtIndex:indexPath.row] objectForKey:@"text"] getHeightOfTextForFontSize:12.0 withLabelWidth:271*fltMultiplyingFactor];
    return 50+hight;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
   // return UITableViewCellEditingStyleDelete;
   
    NSString *userId=[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"];

        if ([[[dataArray objectAtIndex:indexPath.row] objectForKey:@"user_id"]isEqualToString:userId])
        {
            return UITableViewCellEditingStyleDelete;
            
        }
        else{
            return UITableViewCellEditingStyleNone;
        }
    
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
   

    NSString *userId=[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"];
        [tableView beginUpdates];
       if (editingStyle == UITableViewCellEditingStyleDelete)
        {
           
             NSString *commetnID=[[dataArray objectAtIndex:indexPath.row] objectForKey:@"comment_id"];
           
            
             NSDictionary *dataDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                      commetnID,@"comment_id",
                                      userId,@"user_id",nil];
            
            [[Connection sharedConnectionWithDelegate:self] DeleteComment:dataDict];
            [CommentTableViewOutlet reloadData];
            
        }
        [tableView endUpdates];
 
  
    
    
}


#pragma mark - TextFeild delegate Methods -


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
 
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
   
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
        return YES;
}
-(void)setTextFieldPadding:(UITextField *)textField{
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, textField.frame.size.height)];
    leftView.backgroundColor = textField.backgroundColor;
    textField.leftView = leftView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}


- (IBAction)btnSendAction:(id)sender {

     [txtAddComment resignFirstResponder];
    
    if ([[txtAddComment.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
         [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Please add your comment"];
    }
    else {
         NSData *dataComment=[txtAddComment.text dataUsingEncoding:NSUTF8StringEncoding];
        NSString *strComment = [Base64 encode:dataComment];

        NSString *userId=[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"];
        [APPDELEGATE ShowHUDWith:@"Loading..."];
         NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:strPhotoID,@"photo_id",
                            strComment,@"comment"
                            ,userId,@"user_id",
                            nil];
        [[Connection sharedConnectionWithDelegate:self]AddComment:dict];
    }
}

#pragma mark Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    CommentTableViewOutlet.hidden = NO;
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
         if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEGETCOMMENT]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    dataArray =[dataDict objectForKey:@"data"];
                    [CommentTableViewOutlet reloadData];
                    int lastRowNumber = (int)[self.CommentTableViewOutlet numberOfRowsInSection:0] - 1;
                    if (lastRowNumber>0) {
                        NSIndexPath* ip = [NSIndexPath indexPathForRow:lastRowNumber inSection:0];
                        [self.CommentTableViewOutlet scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:NO];
                    }
                    
                }
                else{
                    [dataArray removeAllObjects];
                     [CommentTableViewOutlet reloadData];
                 }
                [SVProgressHUD dismiss];
            }
            else if ([nState isEqualToString:STATEADDCOMMENT]){
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    txtAddComment.text=@"";
                    [self callCommentListData];
                }
                else{
                    
                    [SVProgressHUD dismiss];
                }
             }
            else if ([nState isEqualToString:STATEDELETECOMMENT]){
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                     [CommentTableViewOutlet reloadData];
                    [self callCommentListData];
                }
                else{
                    
                     [SVProgressHUD dismiss];
                }
             }
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
         [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
     [SVProgressHUD dismiss];
//    [UIAlertView showAlertViewWithTitle:ALERTTITLE message:nData];
}


@end
