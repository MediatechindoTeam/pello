//
//  SinglePostVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 7/11/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharingActionSheet.h"
#import "CommentVC.h"

@interface SinglePostVC : UIViewController<TTTAttributedLabelDelegate, SharingActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableViewOutlet;
- (IBAction)backBtnAction:(UIButton *)sender;
@property (strong, nonatomic) NSDictionary *postDataDetail;
@property (nonatomic ,strong) SharingActionSheet *sharingSheet;
@property (nonatomic, readwrite) NSMutableArray *commentArray;
@end
