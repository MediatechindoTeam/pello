//
//  LikerListViewController.h
//  PETSocial
//
//  Created by Habibi on 1/26/16.
//  Copyright © 2016 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikerListViewController : UIViewController
@property (nonatomic, readwrite) NSString *itemId;
@property (nonatomic, readwrite) NSString *typeId;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, readwrite) NSMutableArray *mainArray;
@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end
