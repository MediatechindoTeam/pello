//
//  LikedViewController.m
//  PETSocial
//
//  Created by Habibi on 1/25/16.
//  Copyright © 2016 Ravi B. All rights reserved.
//

#import "LikedViewController.h"

@interface LikedViewController ()

@end

@implementation LikedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.mainArray = [[NSMutableArray alloc] init];
    
    self.indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicatorView.center = self.view.center;
    [self.view addSubview:self.indicatorView];
    [self.indicatorView startAnimating];
    [self.indicatorView setHidden:NO];
    [self.collectionView setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    
}

- (void)reinit
{
    if (self.itemId) {
        [self.indicatorView setHidden:NO];
        [self callLikerList];
    }
}

- (void)callLikerList
{
    NSString *strMyUserID=[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"];
    NSMutableDictionary *dict=[NSMutableDictionary dictionaryWithObjectsAndKeys:
                        strMyUserID,@"user_id",
                        self.typeId,@"type_id",
                        @"liker",@"action",
                        nil];
    
    if ([self.typeId isEqualToString:@"photo"]) {
        [dict setObject:self.itemId forKey:@"photo_id"];
    } else {
        [dict setObject:self.itemId forKey:@"video_id"];
    }
    
    [[Connection sharedConnectionWithDelegate:self] getLikerOfPost:dict];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    [self.indicatorView setHidden:YES];
    [self.collectionView setHidden:NO];
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATELIKER]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES) {
                    self.mainArray = [dataDict objectForKey:@"data"];
                    
                    /*if (self.mainArray.count>0 && self.mainArray.count<4) {
                        NSDictionary *tmp = [self.mainArray objectAtIndex:0];
                        for (int i=1;i<50;i++) {
                            [self.mainArray addObject:tmp];
                        }
                    }*/
                    
                    [self.collectionView reloadData];
                    
                    
                } else {
                    
                }
                
                [SVProgressHUD dismiss];
            }
            
        } else if (strstatuscode == 500) {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        } else {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
        [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
    [SVProgressHUD dismiss];
    [self.indicatorView setHidden:NO];
    //[UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Request Timed Out"];
}




#pragma mark - UICollectionView Delegate and Data Source Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.mainArray count];
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"LikerCell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    NSDictionary *dictData = [self.mainArray objectAtIndex:indexPath.row];
    UILabel *title = (UILabel*)[cell viewWithTag:2];
    title.text = [dictData objectForKey:@"user_name"];
    
    UIImageView *imageView = (UIImageView*)[cell viewWithTag:1];
    NSString *strimageURL=[NSString stringWithFormat:@"%@/%@",REAL_BASE_URL,[dictData objectForKey:@"user_image"]];
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL]
                                                                     cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [imageView setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"image_626"] success:nil failure:nil];
    
    imageView.layer.borderWidth = 1.0f;
    imageView.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor ];
    imageView.layer.masksToBounds = NO;
    imageView.clipsToBounds = YES;
    [imageView.layer setCornerRadius:imageView.frame.size.width/2];
    
    
    int pages = ceil(self.collectionView.contentSize.width /
                     self.collectionView.frame.size.width);
    
    [self.pageControl setNumberOfPages:pages];
    
    return cell;
}

- (void)collectionView:(UICollectionView*)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(70, 70);
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.collectionView.frame.size.width;
    float currentPage = self.collectionView.contentOffset.x / pageWidth;
    
    if (0.0f != fmodf(currentPage, 1.0f)) {
        self.pageControl.currentPage = currentPage + 1;
    } else {
        self.pageControl.currentPage = currentPage;
    }
    
}

@end
