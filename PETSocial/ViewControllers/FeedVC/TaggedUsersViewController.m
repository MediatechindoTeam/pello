//
//  TaggedUsersViewController.m
//  PETSocial
//
//  Created by milap kundalia on 8/1/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "TaggedUsersViewController.h"
#import "FollowFollowerCell.h"
#import "UserProfileVC.h"
@interface TaggedUsersViewController ()

@end

@implementation TaggedUsersViewController
@synthesize TaggedUsersTblOutlet,arrayData;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self preferredStatusBarStyle];
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    
    
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrayData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID =  @"FollowFollowerCell";
    
    FollowFollowerCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        cell = [[FollowFollowerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.lblUserName.text=[[arrayData objectAtIndex:indexPath.row] objectForKey:@"user_name"];
    
    NSString *strimageURL=[NSString stringWithFormat:@"%@",[[arrayData objectAtIndex:indexPath.row] objectForKey:@"user_image"]];
    
    NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
    [cell.imgUsername setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic_50x50"] success:nil failure:nil];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.imgUsername.clipsToBounds = YES;
    cell.imgUsername.layer.cornerRadius = cell.imgUsername.frame.size.width/2;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[arrayData objectAtIndex:indexPath.row] objectForKey:@"user_id"] isEqualToString:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"]]) {
        [self.tabBarController setSelectedIndex:4];
        
        
        [[APPDELEGATE imagefooter] setImage:[UIImage imageWithContentsOfBundleFileName:@"tab5"]];
        
    }else{
        [self performSegueWithIdentifier:@"SingleProfileVC" sender:indexPath];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"SingleProfileVC"])
    {
        UserProfileVC *userProfile=(UserProfileVC *)[segue destinationViewController];
        NSIndexPath *indexPath =( NSIndexPath *)sender;
        userProfile.userDetails=[arrayData objectAtIndex:indexPath.row];
    }
    
}


- (IBAction)btnBackAcion:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
