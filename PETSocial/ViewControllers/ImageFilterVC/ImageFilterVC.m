//
//  ImageFilterVC.m
//  PETSocial
//
//  Created by Habibi on 12/4/15.
//  Copyright © 2015 Ravi B. All rights reserved.
//

#import "ImageFilterVC.h"
#import "GPUImagePicture.h"
#import "PostVC.h"

@interface ImageFilterVC () {
    GPUImageShowcaseFilterType filterType;
    GPUImageOutput<GPUImageInput> *_filter;
}

@end

@implementation ImageFilterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.mainArray = [NSMutableArray arrayWithArray:@[@"Normal",@"Breeze",@"Retro",@"Cloudy",@"Sunset",@"Tune",@"Vanilla",@"Haze",@"Iris",@"Popstar",@"Nostalgia",@"Clear",@"Saturn",@"Expired",@"Coffee",@"Sunny",@"Lemon",@"80's Film",@"80's Magenta",@"Cobalt",@"Morning Beacon",@"Morning Cool",@"Negative",@"Strong Reddish",@"Summer Retro",@"Sunlit",@"Washed"]];
    
    if (self.selectedImage) {
        self.imageView.image = self.selectedImage;
        self.filteredImage = self.selectedImage;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (GPUImageOutput<GPUImageInput>*)processReturnedFilterForIndex:(NSInteger)filterIndex
{
    GPUImageOutput<GPUImageInput>* paramFilter = nil;
    NSString *acvName=nil;
    
    if (filterIndex==GPUIMAGE_NORMAL) {
        
    }
    
    switch (filterIndex) {
        case GPUIMAGE_ACV_BREEZE:  acvName = @"Breeze"; break;
        case GPUIMAGE_ACV_RETRO:  acvName = @"Retro"; break;
        case GPUIMAGE_ACV_CLOUDY:  acvName = @"Cloudy"; break;
        case GPUIMAGE_ACV_SUNSET:  acvName = @"Sunset"; break;
        case GPUIMAGE_ACV_TUNE:  acvName = @"Tune"; break;
        case GPUIMAGE_ACV_VANILLA:  acvName = @"Vanilla"; break;
        case GPUIMAGE_ACV_HAZE:  acvName = @"Haze"; break;
        case GPUIMAGE_ACV_IRIS:  acvName = @"Iris"; break;
        case GPUIMAGE_ACV_POPSTAR:  acvName = @"Popstar"; break;
        case GPUIMAGE_ACV_NOSTALGIA:  acvName = @"Nostalgia"; break;
        case GPUIMAGE_ACV_CLEAR:  acvName = @"Clear"; break;
        case GPUIMAGE_ACV_SATURN:  acvName = @"Saturn"; break;
        case GPUIMAGE_ACV_EXPIRED:  acvName = @"Expired"; break;
        case GPUIMAGE_ACV_COFFEE:  acvName = @"Coffee"; break;
        case GPUIMAGE_ACV_SUNNY:  acvName = @"Sunny"; break;
        case GPUIMAGE_ACV_LEMON:  acvName = @"Lemon"; break;
        case GPUIMAGE_ACV_80FILM: acvName = @"80's Film"; break;
        case GPUIMAGE_ACV_80MAGENTA: acvName = @"80's Magenta"; break;
        case GPUIMAGE_ACV_COBALT: acvName = @"Cobalt"; break;
        case GPUIMAGE_ACV_MORNINGBEACON: acvName = @"Morning Beacon"; break;
        case GPUIMAGE_ACV_MORNINGCOOL: acvName = @"Morning Cool"; break;
        case GPUIMAGE_ACV_NEGATIVE: acvName = @"Negative"; break;
        case GPUIMAGE_ACV_STRONGREDDISH: acvName = @"Strong Reddish"; break;
        case GPUIMAGE_ACV_SUMMERRETRO: acvName = @"Summer Retro"; break;
        case GPUIMAGE_ACV_SUNLIT: acvName = @"Sunlit"; break;
        case GPUIMAGE_ACV_WASHED: acvName = @"Washed"; break;
        default:break;
    }
    
    if (acvName) {
        paramFilter = [[GPUImageToneCurveFilter alloc] initWithACV:acvName];
    }

    return paramFilter;
}

- (void)redrawImage
{
    if (_filter) {
        GPUImagePicture *stillImageSource = [[GPUImagePicture alloc] initWithImage:self.selectedImage];
        
        [stillImageSource addTarget:_filter];
        [_filter useNextFrameForImageCapture];
        [stillImageSource processImage];
        
        self.filteredImage = [_filter imageFromCurrentFramebuffer];
        self.imageView.image = self.filteredImage;
    } else {
        self.imageView.image = self.selectedImage;
    }
}

- (UIImage*)processImage:(UIImage*)thisImage withFilterIndex:(NSInteger)filterIndex
{
    GPUImageOutput<GPUImageInput> *tmpfilter = [self processReturnedFilterForIndex:filterIndex];
    GPUImagePicture *stillImageSource = [[GPUImagePicture alloc] initWithImage:thisImage];
    
    [stillImageSource addTarget:tmpfilter];
    [tmpfilter useNextFrameForImageCapture];
    [stillImageSource processImage];
    
    return [tmpfilter imageFromCurrentFramebuffer];
}

- (IBAction)nextButtonDidPushed:(id)sender {
    [self performSegueWithIdentifier:@"CaptionSegue" sender:self];
}

- (IBAction)doneButtonDidPushed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(imageFilterVCDidDismissWithImage:)]) {
        [self.delegate imageFilterVCDidDismissWithImage:(self.filteredImage) ? self.filteredImage : self.selectedImage];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnBackAcion:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"CaptionSegue"]) {
        PostVC *postVC=[segue destinationViewController];
        postVC.selectedImage = self.filteredImage;
    }
}

#pragma mark - UICollectionView Delegate and Data Source Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return GPUIMAGE_NUMFILTERS;;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"filterCell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    UILabel *title = (UILabel*)[cell viewWithTag:2];
    title.text = [self.mainArray objectAtIndex:indexPath.row];
    
    UIImageView *imageView = (UIImageView*)[cell viewWithTag:1];
    if (indexPath.row > 0) {
        UIImage *filteredImage = [self processImage:self.selectedImage withFilterIndex:indexPath.row];
        imageView.image = filteredImage;
    } else {
        imageView.image = self.selectedImage;
    }
    
    
    return cell;
}

- (void)collectionView:(UICollectionView*)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    _filter = nil;
    _filter = [self processReturnedFilterForIndex:indexPath.row];
    
    [self redrawImage];
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(120, 120);
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

@end
