//
//  ForgotPasswordVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/18/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "ForgotPasswordVC.h"

@interface ForgotPasswordVC ()

@end

@implementation ForgotPasswordVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.txtEmailAddress becomeFirstResponder];
     self.txtEmailAddress.autocorrectionType=UITextAutocorrectionTypeNo;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)submitForWebcall{
    [self.txtEmailAddress resignFirstResponder];
    if (self.txtEmailAddress.text.length!=0) {
        if ([Constant validateEmail:self.txtEmailAddress.text]) {
             [APPDELEGATE ShowHUDWith:@"Loading..."];
            NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:self.txtEmailAddress.text,@"email", nil];
            [[Connection sharedConnectionWithDelegate:self]forgotPasswordWithData:dict];
        }else{
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:ALERTEMAILINVALID];
        }
    }
    else{
        [UIAlertView showAlertViewWithTitle:ALERTTITLE message:ALERTEMAILBLANK];
    }
}
- (IBAction)backBtnAction:(UIButton *)sender {
    [self.txtEmailAddress resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)submitBtnAction:(UIButton *)sender {
    [self submitForWebcall];
}
#pragma mark UITextField Delegate method
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self submitForWebcall];
    return YES;
}

#pragma mark Connection Delegate methods -

- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
         if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEFORGOTPASSWORD]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    
                    UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:ALERTTITLE message:[dataDict valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    alertView.tag=123;
                    [alertView show];
                    
                }
                else{
                    [UIAlertView showAlertViewWithTitle:ALERTTITLE message:[dataDict valueForKey:@"message"]];
                }
                [SVProgressHUD dismiss];
            }
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
            
        }
        else
        {
            [SVProgressHUD dismiss];
            
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
            
        }
    }
    @catch (NSException *exception)
    {
         [SVProgressHUD dismiss];
    }
    
    
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
     [SVProgressHUD dismiss];
 }
#pragma mark UIALertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==123) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
