//
//  PrivacyPolicyViewController.h
//  PETSocial
//
//  Created by milap kundalia on 8/4/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacyPolicyViewController : UIViewController<UIWebViewDelegate>
@property IBOutlet UIWebView *webviewPrivacyPolicy;
@property IBOutlet UIActivityIndicatorView *activityLoader;
- (IBAction)backBtnAction:(UIButton *)sender;
@end
