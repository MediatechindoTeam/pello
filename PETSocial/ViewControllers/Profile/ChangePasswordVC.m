//
//  ChangePasswordVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 8/4/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "ChangePasswordVC.h"

@interface ChangePasswordVC ()

@end

@implementation ChangePasswordVC
@synthesize scrollView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self preferredStatusBarStyle];
    // Do any additional setup after loading the view.
    NSArray *arr = @[self.txtOldPassword, self.txtNewPassword, self.txtConfirmPassword];
    [self setKeyboardControls:[[BSKeyboardControls alloc]initWithFields:arr]];
    [self.keyboardControls setDelegate:self];
    if (iPhone4) {
        scrollView.frame=CGRectMake(0, 64, 320, 504);
    }
    if(iPhone4)
        scrollView.contentSize=CGSizeMake(0,600);


}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(BOOL)isValidate{
    if (self.txtOldPassword.text.length>0) {
        if (self.txtNewPassword.text.length>0) {
            if (self.txtNewPassword.text.length>=6) {
                if ([self.txtNewPassword.text isEqualToString:self.txtConfirmPassword.text]) {
                    return YES;
                }
                else{
                    [UIAlertView showAlertViewWithTitle:@"Pello" message:@"Password doesn't match."];
                }
            }
            else{
                [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Please enter minimum six character in password"];
            }
        }
        else{
            [UIAlertView showAlertViewWithTitle:@"Pello" message:@"Please enter new password."];
        }
    }
    else{
        [UIAlertView showAlertViewWithTitle:@"Pello" message:@"Please enter old password."];
    }
    return NO;
}
- (IBAction)changePasswordBtnAction:(UIButton *)sender {
    if ([self isValidate]) {
        NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"],@"user_id",self.txtOldPassword.text,@"oldpassword",self.txtNewPassword.text,@"newpassword", nil];
        [[Connection sharedConnectionWithDelegate:self] changePassword:dict];
    }
}

- (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark Textfield Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    scrollView.contentOffset = CGPointMake(0,0);

    return YES;
}
- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControl
{
      scrollView.contentOffset = CGPointMake(0,0);
    [keyboardControl.activeField resignFirstResponder];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
     [UIView commitAnimations];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (iPhone4)
    {
        if (textField == self.txtConfirmPassword)
        {
        scrollView.contentOffset = CGPointMake(0,100);
        }else{
            scrollView.contentOffset = CGPointMake(0,0);

        }
    }
    [self.keyboardControls setActiveField:textField];
}
#pragma mark - Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
         if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATECHANGEPASSWORD]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                     [self.navigationController popViewControllerAnimated:YES];
                }
                else{
                     self.txtConfirmPassword.text=@"";
                    self.txtNewPassword.text=@"";
                    self.txtOldPassword.text=@"";
                }
                [SVProgressHUD dismiss];
            }
            
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
         [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
     [SVProgressHUD dismiss];
 }

@end
