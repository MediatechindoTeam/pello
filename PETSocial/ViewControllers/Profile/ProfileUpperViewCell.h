//
//  ProfileUpperViewCell.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 8/27/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileUpperViewCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UIImageView *imgProfilepic;
@property (weak, nonatomic) IBOutlet UIView *viewPost;
@property (weak, nonatomic) IBOutlet UIView *viewFollower;
@property (weak, nonatomic) IBOutlet UIView *viewFollowing;
@property (weak, nonatomic) IBOutlet UILabel *lblPostNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblFollowerNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblFollowingNumber;
@property (weak, nonatomic) IBOutlet UILabel *petnameLabel;
@property (weak, nonatomic) IBOutlet UIButton *petnameButton;

@property (weak, nonatomic) IBOutlet UIButton *twitterUsernameText;
@property (weak, nonatomic) IBOutlet UIButton *websiteURLText;
@property (weak, nonatomic) IBOutlet UILabel *descriptionText;

@property (weak, nonatomic) IBOutlet UIButton *editProfileBtn;

@property (weak, nonatomic) IBOutlet UIButton *followUnfollowBtn;

@property (weak, nonatomic) IBOutlet UIImageView *segmentImageOutlet;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentBtnOutlet;



@end
