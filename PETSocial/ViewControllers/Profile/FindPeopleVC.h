//
//  FindPeopleVC.h
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/24/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FindPeopleVC : UIViewController
- (IBAction)findFacebookBtnAction:(UIButton *)sender;
- (IBAction)backBtnAction:(UIButton *)sender;

@end
