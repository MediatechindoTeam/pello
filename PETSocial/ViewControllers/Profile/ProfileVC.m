//
//  ProfileVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/12/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "ProfileVC.h"
#import "FollowFollowerVC.h"
#import "ProfileCollectionCell.h"
#import "SinglePostVC.h"
#import "FeedVC.h"
#import "ExploreCell.h"
#import "ProfileUpperViewCell.h"
#import "EditProfileVC.h"

@interface ProfileVC ()
{
    NSString *strScreenName;
    NSMutableArray *usersPost;
    NSMutableArray *taggedPost;
    NSDictionary *userDetails;
    NSString *strMyUserID;
    BOOL isMoreData;
    BOOL isValidateData;
    UIRefreshControl *refreshControl;
    
}
@end

@implementation ProfileVC
#pragma mark - View Life Cycle Methods -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self preferredStatusBarStyle];
    
    [self setUpPulltoRefreshView];
    strMyUserID=[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"];
    [self callForUserDetail];
}

-(void)viewWillAppear:(BOOL)animated
{
    AppDelegate *appdelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if (appdelegate.isProfileEdited==YES) {
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(getLoderOpened) userInfo:nil repeats:NO];
        [self callForUserDetail];
    }
}

-(void)setUpPulltoRefreshView{
    refreshControl= [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl setTintColor:[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]];
    
    NSDictionary *refreshAttributes = @{
                                        NSForegroundColorAttributeName: [UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0],
                                        };
    NSString *s = @"Refreshing...";
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:s];
    
    [attributeString setAttributes:refreshAttributes range:NSMakeRange(0, attributeString.length)];
    refreshControl.attributedTitle=attributeString;
    
    
    [refreshControl addTarget:self action:@selector(refershControlAction) forControlEvents:UIControlEventValueChanged];
    [self.collectionViewOutlet addSubview:refreshControl];
    self.collectionViewOutlet.alwaysBounceVertical = YES;
}

-(void)refershControlAction{
    isValidateData=NO;
    [self callForUserDetail];
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [SVProgressHUD dismiss];
        [refreshControl endRefreshing];
        
    });
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Custom Methods -

//http://inheritx.dnsdynamic.com:8590/petsocialnew/index.php?do=/webservice/userinfo
-(void)callForUserDetail{
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:strMyUserID,@"user_id",strMyUserID,@"my_user_id", nil];
    [[Connection sharedConnectionWithDelegate:self] getUserDetail:dict];
}

-(void)callForPostAndTaggedPost:(int) pageNumber{
    
    if (self.segmentBtnOutlet.selectedSegmentIndex==0) {
        NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:strMyUserID,@"my_user_id",strMyUserID,@"user_id",[NSString stringWithFormat:@"%d",pageNumber],@"page", nil];
        [[Connection sharedConnectionWithDelegate:self] getPostOfUser:dict];
    }
    else if (self.segmentBtnOutlet.selectedSegmentIndex==1) {
        NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:strMyUserID,@"my_user_id",strMyUserID,@"user_id",[NSString stringWithFormat:@"%d",pageNumber],@"page", nil];
        [[Connection sharedConnectionWithDelegate:self] getTaggedPostOfUser:dict];
    }
}

-(void)allPostTapAction:(UITapGestureRecognizer *)sender{
    
    
    NSIndexPath*indxPath=[NSIndexPath indexPathForRow:0 inSection:1];
    [_collectionViewOutlet scrollToItemAtIndexPath:indxPath atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
    
}
-(void)followTapAction:(UITapGestureRecognizer *)sender{
    if ([[userDetails objectForKey:@"total_follower"] integerValue]>0) {
        strScreenName=@"Follower";
        [self performSegueWithIdentifier:@"profileToFreind" sender:sender];
        
    }
}

-(void)followingTapAction:(UITapGestureRecognizer *)sender{
    if ([[userDetails objectForKey:@"total_following"] integerValue]>0) {
        strScreenName=@"Following";
        [self performSegueWithIdentifier:@"profileToFreind" sender:sender];
    }
}


#pragma mark - Navigation -

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SinglePostVC"]) {
        NSIndexPath *indexPath=(NSIndexPath *)sender;
        SinglePostVC *singlePost=[segue destinationViewController];
        
        if (self.segmentBtnOutlet.selectedSegmentIndex==0) {
            NSDictionary *obj = [usersPost objectAtIndex:indexPath.item];
            singlePost.typeItem = ([obj objectForKey:@"photo_id"]) ? @"photo" : @"video";
            singlePost.postDataDetail=[usersPost objectAtIndex:indexPath.item];
            
        }else if (self.segmentBtnOutlet.selectedSegmentIndex==1){
            NSDictionary *obj = [taggedPost objectAtIndex:indexPath.item];
            singlePost.typeItem = ([obj objectForKey:@"photo_id"]) ? @"photo" : @"video";
            singlePost.postDataDetail=[taggedPost objectAtIndex:indexPath.item];
        }
    }else if ([[segue identifier] isEqualToString:@"profileToFreind"]) {
        FollowFollowerVC *followFolloerVC=[segue destinationViewController];
        followFolloerVC.strUserId=[userDetails objectForKey:@"user_id"];
        [followFolloerVC setStrScreenName:strScreenName];
    }
    else if ([[segue identifier] isEqualToString:@"AllPostForUsers"]) {
        FeedVC *feedVC=[segue destinationViewController];
        feedVC.allPostForUser=YES;
        feedVC.userProfileId=[[USERDEFAULTS objectForKey:KEYUSERINFO] objectForKey:@"user_id"];
    }
    else if ([[segue identifier]isEqualToString:@"EditProfileVC"]){
        EditProfileVC *editProfile=[segue destinationViewController];
        editProfile.userDetailDict=userDetails;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view cont   roller.
}


- (IBAction)moreBtnAction:(UIButton *)sender {
}
- (IBAction)settingBtnAction:(UIButton *)sender {
    
}

- (void)editProfile:(UIButton *)sender {
    [self performSegueWithIdentifier:@"EditProfileVC" sender:sender];
    
    
}

- (IBAction)segmentBtnAction:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex==0) {
        [self.segmentImageOutlet setImage:[UIImage imageNamed:@"segment1"]];
        [APPDELEGATE ShowHUDWith:@"Loading..."];
        [self callForPostAndTaggedPost:1];
    }
    else if(sender.selectedSegmentIndex==1){
        [self.segmentImageOutlet setImage:[UIImage imageNamed:@"segment2"]];
        [APPDELEGATE ShowHUDWith:@"Loading..."];
        isValidateData=NO;
        [self callForPostAndTaggedPost:1];
    }
}

#pragma mark - UICollectionView Datasource and Delegate -

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat fltMultiplyingFactor = [APPDELEGATE window].frame.size.width / 320;
    
    if (self.segmentBtnOutlet.selectedSegmentIndex==0){
        
        if (indexPath.section==0) {
            return CGSizeMake(320 * fltMultiplyingFactor, 320 * fltMultiplyingFactor);
        }
        else{
            if (indexPath.row==usersPost.count && usersPost.count>collectionItems) {
                return CGSizeMake(300 * fltMultiplyingFactor, 50 * fltMultiplyingFactor);
            }
            else{
                return CGSizeMake(152 * fltMultiplyingFactor, 152 * fltMultiplyingFactor);
            }
        }
    }else{
        
        if (indexPath.section==0) {
            return CGSizeMake(320 * fltMultiplyingFactor, 320 * fltMultiplyingFactor);
        }
        else{
            if (indexPath.row==taggedPost.count && taggedPost.count>collectionItems) {
                return CGSizeMake(300 * fltMultiplyingFactor, 50 * fltMultiplyingFactor);
            }
            else{
                return CGSizeMake(152 * fltMultiplyingFactor, 152 * fltMultiplyingFactor);
            }
        }
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.segmentBtnOutlet.selectedSegmentIndex==0) {
        if (section==0) {
            return 1;
        }
        else{
            if (usersPost.count) {
                if (isMoreData) {
                    return usersPost.count+1;
                }
                else{
                    return usersPost.count;
                }
            }
            else{
                return 0;
            }
        }
        
    }
    else{
        if (section==0) {
            return 1;
        }
        else{
            if (taggedPost.count) {
                if (isMoreData) {
                    return taggedPost.count+1;
                }
                else{
                    return taggedPost.count;
                }
            }
            else{
                return 0;
            }
        }
        
    }
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section!=0) {
        return UIEdgeInsetsMake(5, 5, 5, 5);
    }
    else{
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.segmentBtnOutlet.selectedSegmentIndex==0) {
        if (indexPath.section==0) {
            ProfileUpperViewCell *cell=(ProfileUpperViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ProfileUpperViewCell" forIndexPath:indexPath];
            
            cell.contentView.frame = cell.bounds;
            cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
            
            if ([userDetails objectForKey:@"total_following"]) {
                cell.lblFollowerNumber.text=[NSString stringWithFormat:@"%@",[userDetails objectForKey:@"total_follower"]];
            }
            if ([userDetails objectForKey:@"total_following"]) {
                cell.lblFollowingNumber.text=[NSString stringWithFormat:@"%@",[userDetails objectForKey:@"total_following"]];
            }
            if ([userDetails objectForKey:@"total_posts"]) {
                cell.lblPostNumber.text=[NSString stringWithFormat:@"%@",[userDetails objectForKey:@"total_posts"]];
            }
            
            if ([userDetails objectForKey:@"twitter_username"]) {
                [cell.twitterUsernameText setTitle:[userDetails objectForKey:@"twitter_username"] forState:UIControlStateNormal];
            }
            if ([userDetails objectForKey:@"website_url"]) {
                [cell.websiteURLText setTitle:[userDetails objectForKey:@"website_url"] forState:UIControlStateNormal];
            }
            if ([userDetails objectForKey:@"description"]) {
                cell.descriptionText.text = [userDetails objectForKey:@"description"];
            }
            
            if ([[userDetails objectForKey:@"user_image"] length]>0) {
                NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[userDetails objectForKey:@"user_image"]] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
                [cell.imgProfilepic setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"profilepic"] success:nil failure:nil];
            }
            [cell.editProfileBtn addTarget:self action:@selector(editProfile:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.imgProfilepic.layer.borderWidth = 2.0f;
            cell.imgProfilepic.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor ];
            cell.imgProfilepic.layer.masksToBounds = NO;
            cell.imgProfilepic.clipsToBounds = YES;
            cell.imgProfilepic.layer.cornerRadius = cell.imgProfilepic.frame.size.width/2;
            
            UITapGestureRecognizer *followTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(followTapAction:)];
            [cell.viewFollower addGestureRecognizer:followTap];
            
            UITapGestureRecognizer *followingTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(followingTapAction:)];
            [cell.viewFollowing addGestureRecognizer:followingTap];
            
            if (usersPost.count>0) {
                UITapGestureRecognizer *allPost=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(allPostTapAction:)];
                [cell.viewPost addGestureRecognizer:allPost];
            }
            
            //cell.petnameLabel.text = [userDetails objectForKey:@"pet_name"];
            if ([userDetails objectForKey:@"pet_name"]) {
                [cell.petnameButton setHidden:NO];
                [cell.petnameButton setTitle:[userDetails objectForKey:@"pet_name"] forState:UIControlStateNormal];
            } else {
                [cell.petnameButton setHidden:YES];
            }
            
            
            [cell.segmentBtnOutlet addTarget:self action:@selector(segmentBtnAction:) forControlEvents:UIControlEventValueChanged];
            self.segmentBtnOutlet= cell.segmentBtnOutlet;
            self.segmentImageOutlet=cell.segmentImageOutlet;
            return cell;
        }
        else{
            if (indexPath.item==usersPost.count)
            {
                ExploreCell *cell = (ExploreCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SeeMoreCell" forIndexPath:indexPath];
                
                cell.contentView.frame = cell.bounds;
                cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
                
                cell.grayIndicatorOutlet.frame=CGRectMake(cell.frame.size.width/2, cell.frame.size.height/2, cell.grayIndicatorOutlet.frame.size.width, cell.grayIndicatorOutlet.frame.size.height);
                [cell.grayIndicatorOutlet startAnimating];
                return cell;
                
            }
            else{
                ProfileCollectionCell *myCell = (ProfileCollectionCell *)[collectionView
                                                                          dequeueReusableCellWithReuseIdentifier:@"MyCell"
                                                                          forIndexPath:indexPath];
                
                myCell.contentView.frame = myCell.bounds;
                myCell.contentView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
                
                NSDictionary *dict=[usersPost objectAtIndex:indexPath.row];
                NSString *strimageURL=[NSString stringWithFormat:@"%@",[dict objectForKey:@"post_image"]];
                //for caching image is necessory then use this.
                NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
                [myCell.imgPetOutlet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"thumbnail_300"] success:nil failure:nil];
                
                return myCell;
            }
        }
        
    }
    else if(self.segmentBtnOutlet.selectedSegmentIndex==1){
        if (indexPath.section==0) {
            ProfileUpperViewCell *cell=(ProfileUpperViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ProfileUpperViewCell" forIndexPath:indexPath];
            cell.contentView.frame = cell.bounds;
            cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
            
            cell.lblFollowerNumber.text=[NSString stringWithFormat:@"%@",[userDetails objectForKey:@"total_follower"]];
            cell.lblFollowingNumber.text=[NSString stringWithFormat:@"%@",[userDetails objectForKey:@"total_following"]];
            cell.lblPostNumber.text=[NSString stringWithFormat:@"%@",[userDetails objectForKey:@"total_posts"]];
            
            if ([[userDetails objectForKey:@"user_image"] length]>0) {
                [cell.imgProfilepic setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[userDetails objectForKey:@"user_image"]]]]];
            }
            cell.imgProfilepic.layer.borderWidth = 2.0f;
            cell.imgProfilepic.layer.borderColor = [[UIColor colorWithRed:51.0/255.0f green:176.0/255.0f blue:222.0/255.0f alpha:1.0]CGColor ];
            cell.imgProfilepic.layer.masksToBounds = NO;
            cell.imgProfilepic.clipsToBounds = YES;
            cell.imgProfilepic.layer.cornerRadius = cell.imgProfilepic.frame.size.width/2;
            
            UITapGestureRecognizer *followTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(followTapAction:)];
            [cell.viewFollower addGestureRecognizer:followTap];
            
            UITapGestureRecognizer *followingTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(followingTapAction:)];
            [cell.viewFollowing addGestureRecognizer:followingTap];
            
            if (usersPost.count>0) {
                UITapGestureRecognizer *allPost=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(allPostTapAction:)];
                [cell.viewPost addGestureRecognizer:allPost];
            }
            
            return cell;
        }
        else{
            if (indexPath.item==taggedPost.count)
            {
                ExploreCell *cell = (ExploreCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SeeMoreCell" forIndexPath:indexPath];
                
                cell.contentView.frame = cell.bounds;
                cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
                cell.grayIndicatorOutlet.frame=CGRectMake(cell.frame.size.width/2, cell.frame.size.height/2, cell.grayIndicatorOutlet.frame.size.width, cell.grayIndicatorOutlet.frame.size.height);
                [cell.grayIndicatorOutlet startAnimating];
                return cell;
                
            }
            else{
                ProfileCollectionCell *myCell = (ProfileCollectionCell *)[collectionView
                                                                          dequeueReusableCellWithReuseIdentifier:@"MyCell"
                                                                          forIndexPath:indexPath];
                myCell.contentView.frame = myCell.bounds;
                myCell.contentView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
                NSDictionary *dict=[taggedPost objectAtIndex:indexPath.row];
                NSString *strimageURL=[NSString stringWithFormat:@"%@",[dict objectForKey:@"post_image"]];
                //for caching image is necessory then use this.
                NSMutableURLRequest *imageRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:strimageURL] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15];
                [myCell.imgPetOutlet setImageWithURLRequest:imageRequest placeholderImage:[UIImage imageWithContentsOfBundleFileName:@"thumbnail_300"] success:nil failure:nil];
                return myCell;
            }
            
        }
    }
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section!=0) {
        NSDictionary *dict = [usersPost objectAtIndex:indexPath.row];
        /*if ([dict objectForKey:@"video_id"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"PELLO" message:@"Not supported yet" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            return;
        }*/
        [self performSegueWithIdentifier:@"SinglePostVC" sender:indexPath];
    }
    
}


#pragma mark - Pagination -
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    float reload_distance = 10;
    if(y > h + reload_distance)
    {
        if (self.newPage>0 && isMoreData) {
            isMoreData=NO;
            isValidateData=YES;
            [self callForPostAndTaggedPost:self.newPage];
        }
        //Put your load more data method here...
    }
}

#pragma mark - Connection Delegate methods -
- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
        if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEALLPOST]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    
                    if (!isValidateData) {
                        
                        usersPost=[[NSMutableArray alloc]init];
                        usersPost=[dataDict objectForKey:@"data"];
                        
                    }
                    else{
                        usersPost=[[usersPost arrayByAddingObjectsFromArray:[dataDict objectForKey:@"data"]] mutableCopy];
                    }
                    self.newPage=[[dataDict objectForKey:@"page"] intValue];
                    if (self.newPage>1) {
                        isMoreData=YES;
                    }else{
                        isMoreData=NO;
                    }
                    
                    [self.collectionViewOutlet reloadData];
                }
                else{
                }
                [refreshControl endRefreshing];
                if (usersPost.count==0) {
                }
                else{
                    [self.collectionViewOutlet setScrollEnabled:YES];
                }
                [self.collectionViewOutlet reloadData];
                [SVProgressHUD dismiss];
            }
            else if ([nState isEqualToString:STATETAGGEDPOST]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES)
                {
                    if (!isValidateData) {
                        taggedPost=[[NSMutableArray alloc]init];
                        taggedPost=[dataDict objectForKey:@"data"];
                    }
                    else{
                        taggedPost=[[taggedPost arrayByAddingObjectsFromArray:[dataDict objectForKey:@"data"]] mutableCopy];
                    }
                    self.newPage=[[dataDict objectForKey:@"page"] intValue];
                    if (self.newPage>1) {
                        isMoreData=YES;
                    }
                    else{
                        isMoreData=NO;
                    }
                }
                else{
                }
                [refreshControl endRefreshing];
                if (taggedPost.count==0) {
                    [self.collectionViewOutlet setScrollEnabled:NO];
                }
                else{
                    [self.collectionViewOutlet setScrollEnabled:YES];
                }
                [self.collectionViewOutlet reloadData];
                [SVProgressHUD dismiss];
            }
            else if ([nState isEqualToString:STATEUSERDETAIL]){
                
                /*
                 birthday = "12-01-1900";
                 email = "habibi@mediatechindo.com";
                 "facebook_id" = 0;
                 follower = 0;
                 following = 0;
                 "full_name" = habibi;
                 gender = 1;
                 hometown = "";
                 notification = 0;
                 "pet_name" = "";
                 "total_follower" = 0;
                 "total_following" = 0;
                 "total_posts" = 3;
                 "user_id" = 118;
                 "user_image" = "";
                 "user_name" = "profile-118";
                 "user_type" = 2;
                */
                
                if ([[dataDict valueForKey:@"IsSuccess"]boolValue]==YES) {
                    userDetails=[dataDict objectForKey:@"data"];
                    [self callForPostAndTaggedPost:1];
                    self.profileHeader.text=[[userDetails objectForKey:@"user_name"] uppercaseString];
                    [self.collectionViewOutlet reloadData];
                }
                else{
                    [refreshControl endRefreshing];
                }
                [SVProgressHUD dismiss];
            }
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
        [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
    [SVProgressHUD dismiss];
    [refreshControl endRefreshing];
}

-(void)getLoderOpened
{
    [APPDELEGATE ShowHUDWith:@"Loading..."];
    [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(getLoderClosed) userInfo:nil repeats:NO];
}
-(void)getLoderClosed
{
    [SVProgressHUD dismiss];
}

@end
