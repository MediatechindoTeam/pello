//
//  RegistrationVC.m
//  PETSocial
//
//  Created by Ravi Bhavsar on 6/6/14.
//  Copyright (c) 2014 Ravi B. All rights reserved.
//

#import "RegistrationVC.h"

@interface RegistrationVC ()
{
    UITextField *selectedTextField;
}
@end

@implementation RegistrationVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tapForKeyboard = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resignAllKeyboards:)];
    [self.scrollViewOutlet setShowsVerticalScrollIndicator:NO];
    [self.scrollViewOutlet addGestureRecognizer:tapForKeyboard];
    [self.scrollViewOutlet setContentSize:CGSizeMake(320, 450)];
    
    NSArray *arr = @[self.txtEmailAddress, self.txtUserName, self.txtPassword];
    [self setKeyboardControls:[[BSKeyboardControls alloc]initWithFields:arr]];
    [self.keyboardControls setDelegate:self];
    self.txtEmailAddress.autocorrectionType=UITextAutocorrectionTypeNo;
    self.txtUserName.autocorrectionType=UITextAutocorrectionTypeNo;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)resignAllKeyboards:(id)sender {
    // For Tap Gesture
    
    [self.txtEmailAddress resignFirstResponder];
    [self.txtUserName resignFirstResponder];
    [self.txtPassword resignFirstResponder];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    [self.scrollViewOutlet setContentSize:CGSizeMake(320, 450)];
    self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 0);
    [UIView commitAnimations];
}
-(BOOL)validation{
    // this method is for Adding validation
    
    if([self.txtEmailAddress.text length]==0)
    {
        [UIAlertView showAlertViewWithTitle:ALERTTITLE message:ALERTEMAILINVALID];
        return NO;
    }
    if (![Constant validateEmail:self.txtEmailAddress.text]) {
        [UIAlertView showAlertViewWithTitle:ALERTTITLE message:ALERTEMAILINVALID];
        return NO;
    }
    if([self.txtUserName.text length]==0)
    {
        [UIAlertView showAlertViewWithTitle:ALERTTITLE message:ALERTUSERNAMEBLANK];
        return NO;
    }
    
    if (self.txtUserName.text) {
        NSRange whiteSpaceRange = [self.txtUserName.text rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
        if (whiteSpaceRange.location != NSNotFound) {
             [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Space not allowed in username"];
            return NO;
        }
    }
    if([self.txtPassword.text length]==0)
    {
        [UIAlertView showAlertViewWithTitle:ALERTTITLE message:ALERTPASSWORDBLANK];
        return NO;
    }
    if([self.txtPassword.text length]<6)
    {
        [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Please enter minimum six character in password"];
        return NO;
    }       else{
        return YES;
    }
}



#pragma mark - TextFeild delegate Methods -
- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControl{
    
    [keyboardControl.activeField resignFirstResponder];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    [self.scrollViewOutlet setContentSize:CGSizeMake(320, 450)];
    self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 0);
    [UIView commitAnimations];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    selectedTextField = textField;
    [self.scrollViewOutlet setContentSize:CGSizeMake(320, 494+216)];
    //  [textField setInputAccessoryView:toolbarAccessoryView];
    if (iPhone4) {
        if (textField == self.txtEmailAddress) {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3f];
            self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 115);
            [UIView commitAnimations];
        }
        else if (textField == self.txtUserName) {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3f];
            self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 175);
            [UIView commitAnimations];
        } else  if (textField == self.txtPassword) {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3f];
            self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 235);
            [UIView commitAnimations];
            
        }  else {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3f];
            self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 0);
            [UIView commitAnimations];
        }
    }
    else if (iPhone5) {
        if (textField == self.txtEmailAddress) {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3f];
            self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 25);
            [UIView commitAnimations];
        } else if (textField == self.txtUserName) {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3f];
            self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 85);
            [UIView commitAnimations];
        } else  if (textField == self.txtPassword) {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3f];
            self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 145);
            [UIView commitAnimations];
            
        }  else {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3f];
            self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 0);
            [UIView commitAnimations];
        }
    }
    else{
        if (textField == self.txtPassword){
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3f];
            self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 95);
            [UIView commitAnimations];
        }
       
    }
    [self.keyboardControls setActiveField:textField];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //    RegistrationScrollView.contentOffset = CGPointMake(0, 0);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [UIView beginAnimations:Nil context:nil];
    [UIView setAnimationDuration:0.3f];
    [self.scrollViewOutlet setContentSize:CGSizeMake(320, 450)];
    self.scrollViewOutlet.contentOffset = CGPointMake(self.scrollViewOutlet.contentOffset.x, 0);
    [UIView commitAnimations];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

 - (IBAction)backBtnAction:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)btnRegisterAction:(UIButton *)sender {
    
    if ([self validation]) {
        [APPDELEGATE ShowHUDWith:@"Loading..."];
        NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:self.txtEmailAddress.text,@"email",self.txtUserName.text,@"user_name",self.txtPassword.text,@"password",self.txtUserName.text,@"full_name",[USERDEFAULTS objectForKey:@"Devicetoken"],@"cf_device_token", nil];
        [[Connection sharedConnectionWithDelegate:self]registerWithData:dict];
        
    }
}
#pragma mark Connection Delegate methods -

- (void)ConnectionDidFinish:(NSString*)nState Data: (NSString*)nData statuscode:(NSInteger )strstatuscode
{
    @try {
        NSDictionary* dataDict = [[NSDictionary alloc] initWithDictionary:[nData JSONValue]];
        dataDict = [dataDict dictionaryByReplacingNullsWithStrings];
         if (strstatuscode == 200)
        {
            if ([nState isEqualToString:STATEREGISTRATION]) {
                if ([[dataDict valueForKey:@"IsSuccess"] boolValue]==YES )
                {
                    [USERDEFAULTS removeObjectForKey:KEYUSERINFO];
                    [USERDEFAULTS setObject:[dataDict objectForKey:@"data"] forKey:KEYUSERINFO];
                    [USERDEFAULTS setObject:KEYLOGEDINYES forKey:KEYISLOGEDIN];
                    [APPDELEGATE pushTabBar:self.navigationController];
                }
                else{
                    [UIAlertView showAlertViewWithTitle:ALERTTITLE message:[dataDict valueForKey:@"message"]];
                }
                [SVProgressHUD dismiss];
            }
        }
        else if (strstatuscode == 500)
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
        else
        {
            [SVProgressHUD dismiss];
            [UIAlertView showAlertViewWithTitle:ALERTTITLE message:@"Server is not responding."];
        }
    }
    @catch (NSException *exception)
    {
         [SVProgressHUD dismiss];
    }
}
- (void)ConnectionDidFail:(NSString*)nState Data: (NSString*)nData{
     [SVProgressHUD dismiss];
 }

@end
