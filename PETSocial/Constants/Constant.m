//
//  Constant.m
//
//

#import "Constant.h"

@implementation Constant

+ (BOOL) validateEmail: (NSString *) candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

+(NSString *)UTCtoDeviceTimeZone:(NSString *)UTCTime
{
    //Ravi bhavsar
    NSString* input = UTCTime;
    NSString* format = @"yyyy-MM-dd HH:mm:ss";
    
    // Set up an NSDateFormatter for UTC time zone
    NSDateFormatter* formatterUtc = [[NSDateFormatter alloc] init];
    [formatterUtc setDateFormat:format];
    [formatterUtc setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    // Cast the input string to NSDate
    NSDate* utcDate = [formatterUtc dateFromString:input];
    
    // Set up an NSDateFormatter for the device's local time zone
    NSDateFormatter* formatterLocal = [[NSDateFormatter alloc] init];
    [formatterLocal setDateFormat:format];
    [formatterLocal setTimeZone:[NSTimeZone systemTimeZone]];
    
    // Create local NSDate with time zone difference
    //    NSDate* localDate = [formatterUtc dateFromString:[formatterLocal stringFromDate:utcDate]];
    

     
    return [formatterLocal stringFromDate:utcDate];
}

+(NSString *)makeTinyURL:(NSString *)originalURL{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://tinyurl.com/api-create.php?url=%@", originalURL]];
    NSURLRequest *request = [ NSURLRequest requestWithURL:url
                                              cachePolicy:NSURLRequestReloadIgnoringCacheData
                                          timeoutInterval:10.0 ];
    NSError *error;
    NSURLResponse *response;    
    NSData *myUrlData = [ NSURLConnection sendSynchronousRequest:request
                                               returningResponse:&response
                                                           error:&error];
    NSString *myTinyUrl = [[NSString alloc] initWithData:myUrlData encoding:NSUTF8StringEncoding];
    return myTinyUrl;
}
+(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
+ (UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 400; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // [self setRotatedImage:imageCopy];
    return imageCopy;
}
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
+ (UIImage*)cropImage:(UIImage*)imageTaked
{
    UIImage* imageCropped;
    
    CGFloat side = MIN(imageTaked.size.width, imageTaked.size.height);
    //CGFloat side = 600.0f;
    CGFloat x = imageTaked.size.width / 2 - side / 2;
    CGFloat y = imageTaked.size.height / 2 - side / 2;
    
    CGRect cropRect = CGRectMake(x,y,side,side);
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageTaked CGImage], cropRect);
    imageCropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return imageCropped;
}
+ (NSString*) durationbetweendate: (NSString *) candidate
{
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate* now = [NSDate date];
//    NSString *tempCurDate=[formatter stringFromDate:now];
    //    NSString* str = [formatter stringFromDate:now];
    
    NSDateFormatter*dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//2012-09-14 14:16:41
    
    NSDate* date = [dateformatter dateFromString:candidate];
//    NSDate* curDate = [dateformatter dateFromString:tempCurDate];
    NSTimeInterval seconds = [now timeIntervalSinceDate:date];
    NSString* result = @"";
    if (seconds==0) {
        result = @"0 secongs ago";
    }
    else
    {
        int hours = 0;
        int minutes = 0;
        minutes = seconds/60;
        seconds -= minutes*60;
        hours = minutes / 60;
        minutes -= hours*60;
        if (hours<24)
        {
            NSString*strSeconds = [seconds<10?@"":@"" stringByAppendingFormat:@"%.0f",seconds];
            NSString*strMinutes = [minutes<10?@"":@"" stringByAppendingFormat:@"%d",minutes];
            NSString*strHours = [hours<10?@"":@"" stringByAppendingFormat:@"%d",hours];
            
            //hours
            if ([strHours intValue]>0) {
                strHours = [strHours intValue]==1?[NSString stringWithFormat:@"%@ hour ago ",strHours]:[NSString stringWithFormat:@"%@ hours ago ",strHours];
            }
            else {
                strHours =@"";
            }
            
            //minutes
            if ([strMinutes intValue]>0) {
                strMinutes = [strMinutes intValue]==1?[NSString stringWithFormat:@"%@ min ago ",strMinutes]:[NSString stringWithFormat:@"%@ mins ago ",strMinutes];
            }
            else {
                strMinutes =@"";
            }
            
            // Seconds
            if ([strSeconds intValue]>0) {
                strSeconds = [strSeconds intValue]==1?[NSString stringWithFormat:@"%@ sec ago ",strSeconds]:[NSString stringWithFormat:@"%@ secs ago ",strSeconds];
            }
            else {
                strSeconds =@"";
            }
            
            if (![strHours isEqualToString:@""])
            {
                result = strHours;
            }
            else if (![strMinutes isEqualToString:@""])
            {
                result = strMinutes;
            }
            else //if (![strSeconds isEqualToString:@""])
            {
                if (strSeconds.length==0) {
                    result = @"few secs ago";
                }
                else{
                    result = strSeconds;
                }
            }
        }
        else
        {
            int days = hours/24;
            if (days>365)
            {
                int year=days/365;
                if (year>1) {
                    result = [NSString stringWithFormat:@"%d years ago ",year];
                }
                else {
                    result = [NSString stringWithFormat:@"%d year ago ",year];
                }
            }
            else
            {
                if (days>32) {
                    // has to change for month ago.
                    int months = floor(days/30);

                    
                    if (months>1) {
                        result = [NSString stringWithFormat:@"%d months ago ",months];
                    }
                    else {
                        result = [NSString stringWithFormat:@"%d month ago ",months];
                    }
                }
                else{
                    if (days>1) {
                        result = [NSString stringWithFormat:@"%d days ago ",days];
                    }
                    else {
                        result = [NSString stringWithFormat:@"%d day ago ",days];
                    }
                }
            }
        }
    }
    
    return result;
}
@end
