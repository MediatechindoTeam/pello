//
//  SinglePostTableViewCell.h
//  PETSocial
//
//  Created by Habibi on 12/5/15.
//  Copyright © 2015 Ravi B. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SinglePostTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblCommentTime;
@property (strong, nonatomic) IBOutlet UILabel *lblCommentText;
@property (strong, nonatomic) IBOutlet UIButton *btnUserNameBtn;
@end
